<?php
use app\controllers\OrderController;
use app\models\City;
use app\models\Order;
use kartik\grid\GridView;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

	<?php $form = ActiveForm::begin(); ?>
	<div class="portlet box blue clearfix">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i> Product Form
			</div>
			<div class="tools">
				<a href="" class="collapse" data-original-title="" title="">
				</a>
				<a href="#" data-toggle="modal" class="config" data-original-title="" title="">
				</a>
				<a href="" class="reload" data-original-title="" title="">
				</a>
				<a href="" class="remove" data-original-title="" title="">
				</a>
			</div>
		</div>
		<div class="portlet-body form">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-6">
						<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-sm-6">

						<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-6">
						<?= $form->field($model, 'phone')->textInput() ?>
					</div>
					<div class="col-sm-6">

						<?= $form->field($model, 'status')->dropDownList($model::STATUS) ?>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-3"><?= $form->field($model, 'shipping_city_id')->dropDownList(OrderController::getShippingCity(), [
							'id'     => 'city-id',
							'prompt' => 'Select...',
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'shipping_district_id')->widget(DepDrop::classname(), [
							'options'       => ['id' => 'district-id'],
							'pluginOptions' => [
								'depends'     => ['city-id'],
								'placeholder' => 'Select...',
								'url'         => Url::to(['/order/get-district']),
							],
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'shipping_village_id')->widget(DepDrop::classname(), [
							'options'       => ['id' => 'village-id'],
							'pluginOptions' => [
								'depends'     => ['district-id'],
								'placeholder' => 'Select...',
								'url'         => Url::to(['/order/get-village']),
							],
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'shipping_address')->textInput() ?></div>

				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-6">

						<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

					</div>
					<div class="col-sm-6">

						<?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
					</div>
				</div>

			</div>

			<div class="form-group col-sm-12">
				<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>

	</div>
	<?php if(!$model->isNewRecord) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet purple box">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cogs"></i>Danh sách đơn hàng
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="javascript:;" class="reload">
							</a>
							<a href="javascript:;" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<?= GridView::widget([
							'dataProvider' => $dataProvider,
							'columns'      => [
								['class' => 'yii\grid\SerialColumn'],
								[
									'attribute' => 'status',
									'value'     => function (Order $data) {
										return $data::STATUS[$data->status];
									},
								],
								[
									'attribute' => 'customer_id',
									'value'     => function ($data) {
										return Html::a($data->customer->name, Url::to([
											'customer/update',
											'id' => $data->customer->id,
										]));
									},
									'format'=>'raw'
								],
								'shipping_code',
								[
									'attribute' => 'created_by',
									'value'     => function (Order $data) {
										return $data->userCreate->username ?? '';
									},
								],
								'total_amount',
								'total_amount_real',
								'discount',
								'note',
								'created_date',
								//'update_by',
								['class' => 'yii\grid\ActionColumn'],
							],
						]); ?>

					</div>
				</div>
			</div>
		</div>

	<?php } ?>
</div>