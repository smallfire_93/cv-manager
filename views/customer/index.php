<?php
use app\models\Customer;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<div class="row">
		<div class="col-md-6">
			<div class="portlet blue box">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i>Tìm kiếm khách hàng
					</div>
					<div class="tools">
						<a href="javascript:;" class="collapse">
						</a>
						<a href="#portlet-config" data-toggle="modal" class="config">
						</a>
						<a href="javascript:;" class="reload">
						</a>
						<a href="javascript:;" class="remove">
						</a>
					</div>
				</div>
				<div class="portlet-body" id="blockui_sample_1_portlet_body">
					<?php echo $this->render('_search', ['model' => $searchModel]); ?>

				</div>
			</div>
		</div>
	</div>
	<p>
		<?= Html::a('Tạo mới khách hàng', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'phone',
			[
				'attribute' => 'shipping_code',
				'value'     => function (Customer $data) {
					return $data->getAllOrderCode();
				},
			],
			'name',
			[
				'attribute' => 'user_id',
				'value'     => function ($data) {
					return $data->userAdmin ? $data->userAdmin->username : ' Không có';
				},
			],
			//	        'address',
			//            'shipping_address',
			[
				'attribute' => 'total_money',
				'value'     => function (Customer $data) {
					return $data->getAllOrderMoney();
				},
			],
			[
				'attribute' => 'status',
				'value'     => function ($data) {
					return $data::STATUS[$data->status];
				},
				'filter'    => $searchModel::STATUS,
			],
			'note:ntext',
			//'city_id',
			'created_date',
			[
				'class'    => 'yii\grid\ActionColumn',
				'header'   => 'Thông tin chi tiết',
				'template' => '{update}',
			],
		],
	]); ?>
</div>
