<?php
/* @var $this \yii\web\View */
/* @var $content string */
use app\components\widgets\Menu;
use app\components\widgets\LeftSidebar;
use app\components\widgets\TopBar;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="no-js">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

</head>
<body class="page-header">
<?php $this->beginBody() ?>
<?= TopBar::widget() ?>
<div class="clearfix">
</div>
<div class="page-container">

	<div class="page-content-wrapper">
		<div class="page-content clearfix">
			<div class="main-container">
				<?= $content ?>
			</div>
		</div>
	</div>
</div>

<footer class="footer">
	<div class="container">
		<p class="pull-left">&copy; My Company <?= date('Y') ?></p>

		<p class="pull-right"><?= Yii::powered() ?></p>
	</div>
</footer>
<!--<script>-->
<!--    jQuery(document).ready(function() {-->
<!--        Metronic.init(); // init metronic core componets-->
<!--        Layout.init(); // init layout-->
<!--        Demo.init(); // init demo features-->
<!--        Index.init(); // init index page-->
<!--        Tasks.initDashboardWidget(); // init tash dashboard widget-->
<!--    });-->
<!--</script>-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
