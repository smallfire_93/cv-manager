<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 8/1/2018
 * Time: 5:25 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Thêm sản phẩm vào kho';
?>
<h1><?= Html::encode($this->title) ?></h1>

<?php $form = ActiveForm::begin([
	'layout' => 'horizontal',
]); ?>
<?= $form->field($model, 'name')->textInput([
	'disabled' => true,
	'value'    => $product->name,
]) ?>
<?= $form->field($model, 'quantity')->widget('\yii\widgets\MaskedInput', [
	'options'       => [
		'class' => 'col-sm-12 form-control',
	],
	'clientOptions' => [
		'alias'              => 'decimal',
		'groupSeparator'     => ',',
		'autoGroup'          => true,
		'removeMaskOnSubmit' => true,
	],
]) ?>
<div class="form-group col-sm-3">
	<?= Html::submitButton('Add', ['class' => 'btn btn-success pull-right']) ?>
</div>
<?php ActiveForm::end(); ?>
