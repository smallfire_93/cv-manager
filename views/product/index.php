<?php
use app\models\Product;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Thêm mới sản phẩm', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			//			'product_img',
			[
				'attribute' => 'category_id',
				'value'     => function (Product $data) {
					return $data->category->name;
				},
			],
			'name',
			'code',
			//			'image',
			'in_stock',
			[
				'attribute' => 'add_stock',
				'value'     => function ($data) {
					return Html::a('<i class="icon-plus fa-2x"></i>', [
						'/product/add',
						'id' => $data->id,
					]);
				},
				'format'    => 'raw',
			],
			//'in_stock',
			//'base_price',
			//'price',
			//'description:ntext',
			//'created_date',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
</div>
