<?php
use kartik\widgets\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
	//		'layout'  => 'horizontal',
	'id'          => 'product-form',

	'options' => [
		'enctype' => 'multipart/form-data',
	],
]); ?>
<div class="portlet box blue clearfix">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Product Form
		</div>
		<div class="tools">
			<a href="" class="collapse" data-original-title="" title="">
			</a>
			<a href="#" data-toggle="modal" class="config" data-original-title="" title="">
			</a>
			<a href="" class="reload" data-original-title="" title="">
			</a>
			<a href="" class="remove" data-original-title="" title="">
			</a>
		</div>
	</div>
	<div class="portlet-body form">
		<div class="col-sm-6">
			<?= $form->field($model, 'category_id')->dropDownList($model->getCategoryOrder(), ['prompt' => 'Please choose category']) ?>

			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'product_img')->widget(FileInput::className(), [
				'options'       => ['accept' => 'image/*'],
				'pluginOptions' => [
					'allowedFileExtensions' => [
						'jpg',
						'gif',
						'png',
					],
					'showUpload'            => false,
					'initialPreview'        => $model->getIsNewRecord() ? [
						Html::img(Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif', ['class' => 'file-preview-image']),
					] : [
						Html::img($model->getPictureUrl('image'), ['class' => 'file-preview-image']),
					],
				],
			]); ?>
		</div>
		<div class="col-sm-6">
			<?php if($model->getIsNewRecord()) { ?>
				<?= $form->field($model, 'in_stock')->widget('\yii\widgets\MaskedInput', [
					'options'       => [
						'class' => 'col-sm-12 form-control',
					],
					'clientOptions' => [
						'alias'              => 'decimal',
						'groupSeparator'     => ',',
						'autoGroup'          => true,
						'removeMaskOnSubmit' => true,
					],
				]) ?>
				<?php DynamicFormWidget::begin([
					'widgetContainer' => 'dynamicform_wrapper',
					'widgetBody'      => '.container-items',
					'widgetItem'      => '.item',
					'limit'           => 10,
					//		'min'             => 3,
					'insertButton'    => '.add-item',
					'deleteButton'    => '.remove-item',
					'model'           => $modelAttributes[0],
					'formId'          => 'product-form',
					'id'              => 'dynamic-form',
					'formFields'      => [
						'attribute_value_id',
						'quantity',
					],
				]); ?>
				<div class="container-items">
					<?php foreach($modelAttributes as $index => $modelAttribute): ?>
						<div class="row item">
							<div class="col-md-6">
								<?= $form->field($modelAttribute, "[{$index}]attribute_value_id") ?>
							</div>
							<div class="col-md-6">
								<?= $form->field($modelAttribute, "[{$index}]quantity"); ?>
							</div>
						</div>
					<?php endforeach; ?>

				</div>
				<div class="clearfix"></div>
				<button type="button" class="add-item">
					<i class="fa fa-plus"></i>
					Thêm
				</button>

				<?php DynamicFormWidget::end(); ?>
			<?php } ?>
			<?= $form->field($model, 'weight')->widget('kartik\number\NumberControl') ?>

			<?= $form->field($model, 'base_price')->widget('\yii\widgets\MaskedInput', [
				'options'       => [
					'class' => 'col-sm-12 form-control',
				],
				'clientOptions' => [
					'alias'              => 'decimal',
					'groupSeparator'     => ',',
					'autoGroup'          => true,
					'removeMaskOnSubmit' => true,
				],
			]) ?>
			<?= $form->field($model, 'price')->widget('\yii\widgets\MaskedInput', [
				'options'       => [
					'class' => 'col-sm-12 form-control',
				],
				'clientOptions' => [
					'alias'              => 'decimal',
					'groupSeparator'     => ',',
					'autoGroup'          => true,
					'removeMaskOnSubmit' => true,
				],
			]) ?>

			<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
		</div>
		<div class="form-group">
			<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		</div>

	</div>
</div>
<?php ActiveForm::end(); ?>
