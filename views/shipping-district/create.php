<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ShippingDistrict */

$this->title = 'Create Shipping District';
$this->params['breadcrumbs'][] = ['label' => 'Shipping Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-district-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
