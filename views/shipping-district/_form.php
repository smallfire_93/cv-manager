<?php
use app\models\ShippingCity;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShippingDistrict */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shipping-district-form">

	<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'short_code')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'city_shipping')->widget(Select2::className(), [
		'data' => ArrayHelper::map(ShippingCity::find()->all(), 'id', 'name'),
	]) ?>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
