<?php
use app\models\StockLog;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\StockLog */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Lịch sử kho';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-log-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php Pjax::begin(); ?>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
//		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'product_id',
				'value'     => function (StockLog $data) {
					return $data->product->name;
				},
			],
			'quantity',
			'total_price',
			'base_price',
			[
				'attribute' => 'type',
				'value'     => function (StockLog $data) {
					return $data::TYPE[$data->type];
				},
			],
			[
				'attribute' => 'order_id',
				'value'     => function (StockLog $data) {
					return isset($data->orders) ? $data->orders->code : '';
				},
			],
			'created_at'
		],
	]); ?>
	<?php Pjax::end(); ?>
</div>
