<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CvInfo */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cv Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cv-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'birthday',
            'phone',
            'email:email',
            'cv',
            'address',
            'status',
            'gender',
            'interview_day',
            'trial_start',
            'trial_end',
            'official_start',
            'trainer_id',
            'trainer_name',
            'city',
            'hometown',
            'id_number',
            'user_id',
            'password',
            'note:ntext',
            'picture',
        ],
    ]) ?>

</div>
