<?php
use app\models\CvInfo;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CvInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Cv Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cv-info-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php  echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Create Cv Info', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'name',
			'birthday',
			'phone',
			'email:email',
			//'address',
			[
				'attribute' => 'status',
				'value'     => function (CvInfo $data) {
					return $data::STATUS[$data->status];
				},
			],
			[
				'attribute' => 'cv',
				'header'    => 'Download CV',
				'format'    => 'raw',
				'value'     => function (CvInfo $data) {
					return Html::a('Download', Url::to([
						'download',
						'name' => $data->cv,
					], []), ['class' => 'btn btn-success']);
				},
			],
			//'gender',
			//'interview_day',
			//'trial_start',
			//'trial_end',
			//'official_start',
			//'trainer_id',
			//'trainer_name',
			//'city',
			//'hometown',
			//'id_number',
			//'user_id',
			//'password',
			//'note:ntext',
			//'picture',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
</div>
