<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CvInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cv-info-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<div class="col-sm-4">
		<?= $form->field($model, 'name') ?>

	</div>

	<div class="col-sm-4">
		<?php echo $form->field($model, 'status')->dropDownList($model::STATUS) ?>

	</div>

	<div class="col-sm-4">
		<?= $form->field($model, 'phone') ?>

	</div>

	<?php // echo $form->field($model, 'cv') ?>

	<?php // echo $form->field($model, 'address') ?>


	<?php // echo $form->field($model, 'gender') ?>

	<?php // echo $form->field($model, 'interview_day') ?>

	<?php // echo $form->field($model, 'trial_start') ?>

	<?php // echo $form->field($model, 'trial_end') ?>

	<?php // echo $form->field($model, 'official_start') ?>

	<?php // echo $form->field($model, 'trainer_id') ?>

	<?php // echo $form->field($model, 'trainer_name') ?>

	<?php // echo $form->field($model, 'city') ?>

	<?php // echo $form->field($model, 'hometown') ?>

	<?php // echo $form->field($model, 'id_number') ?>

	<?php // echo $form->field($model, 'user_id') ?>

	<?php // echo $form->field($model, 'password') ?>

	<?php // echo $form->field($model, 'note') ?>

	<?php // echo $form->field($model, 'picture') ?>

	<div class="form-group">
		<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
