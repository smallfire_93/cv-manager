<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CvInfo */

$this->title = 'Update Cv Info: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cv Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cv-info-update">

    <h1 class="form-title"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
