<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CvInfo */

$this->title = 'Thêm mới CV';
$this->params['breadcrumbs'][] = ['label' => 'Cv Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cv-info-create">

    <h1 class="form-title"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
