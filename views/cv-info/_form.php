<?php
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CvInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cv-info-form">

	<?php $form = ActiveForm::begin([
		//		'layout'  => 'horizontal',
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]); ?>
	<div class="portlet box blue clearfix">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i> CV Form
			</div>
			<div class="tools">
				<a href="" class="collapse" data-original-title="" title="">
				</a>
				<a href="#" data-toggle="modal" class="config" data-original-title="" title="">
				</a>
				<a href="" class="reload" data-original-title="" title="">
				</a>
				<a href="" class="remove" data-original-title="" title="">
				</a>
			</div>
		</div>
		<div class="portlet-body form">
			<div class="col-sm-4">
				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'birthday')->widget(DatePicker::className(), [
					'pluginOptions' => [
						'autoclose' => true,
						'format'    => 'yyyy-mm-dd',
					],
				]) ?>
				<?= $form->field($model, 'cv_file')->widget(FileInput::className(), [
					//        'options'       => ['accept' => 'image/*'],
					'pluginOptions' => [
						'allowedFileExtensions' => [
							'doc',
							'docx',
							'pdf',
						],
						'showUpload'            => false,
					],
				]); ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'apply_position')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'status')->dropDownList($model::STATUS) ?>

				<?= $form->field($model, 'gender')->dropDownList($model::GENDER) ?>

				<?= $form->field($model, 'interview_day')->widget(DateTimePicker::className(), [
					'pluginOptions' => [
						'todayHighlight' => true,
					],
					'options'       => ['placeholder' => 'Ngày phỏng vấn'],
				])->label(false) ?>
			</div>
			<div class="col-sm-4">

				<?= $form->field($model, 'trial_start')->widget(DatePicker::className(), [
					'pluginOptions' => [
						'autoclose' => true,
						'format'    => 'yyyy-mm-dd',
					],
					'options'       => ['placeholder' => 'Ngày thử việc'],
				])->label(false) ?>

				<?= $form->field($model, 'trial_end')->widget(DatePicker::className(), [
					'pluginOptions' => [
						'autoclose' => true,
						'format'    => 'yyyy-mm-dd',
					],
					'options'       => ['placeholder' => 'Ngày thử việc kết thúc'],
				])->label(false) ?>

				<?= $form->field($model, 'official_start')->widget(DatePicker::className(), [
					'pluginOptions' => [
						'autoclose' => true,
						'format'    => 'yyyy-mm-dd',
					],
					'options'       => ['placeholder' => 'Ngày làm việc chính thức'],
				])->label(false) ?>

				<?= $form->field($model, 'trainer_name')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'city')->widget(Select2::className(), [
					'data' => ArrayHelper::map(\app\models\City::find()->andWhere([
						'status' => 1,
					])->all(), 'id', 'name'),
				])->label('Thành phố') ?>
				<?= $form->field($model, 'hometown')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'id_number')->textInput() ?>

				<?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

				<div class="form-group">
					<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
				</div>
			</div>
		</div>
	</div>
	<?php ActiveForm::end(); ?>

</div>
