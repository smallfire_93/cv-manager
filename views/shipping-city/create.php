<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ShippingCity */

$this->title = 'Create Shipping City';
$this->params['breadcrumbs'][] = ['label' => 'Shipping Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
