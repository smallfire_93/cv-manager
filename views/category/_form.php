<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>


	<div class="category-form">

		<?php $form = ActiveForm::begin([
			'layout' => 'horizontal',
		]); ?>

		<?= $form->field($model, 'parent_id')->dropDownList($model->getCategoryOrder(), ['prompt' => 'Danh mục']) ?>

		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

		<div class="form-group col-sm-4">
			<?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
		</div>


		<?php ActiveForm::end(); ?>

	</div>

