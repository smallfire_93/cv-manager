<?php
use app\components\Model;
use app\controllers\OrderController;
use app\models\Customer;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderForm */
$this->title                   = 'Order: ' . $order->id;
$this->params['breadcrumbs'][] = [
	'label' => 'Orders',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = [
	'label' => $order->id,
	'url'   => [
		'view',
		'id' => $order->id,
	],
];
$this->params['breadcrumbs'][] = 'Update';
Yii::$app->view->registerJs('var getInfoUrl = "' . Url::to(['customer/get-info']) . '"', \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var orderItemUrl = "' . Url::to([
		'/order/order-item',
	]) . '"', \yii\web\View::POS_HEAD);
$this->registerJsFile('@web/js/order.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="order-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php $form = ActiveForm::begin(); ?>
	<div class="portlet box blue clearfix">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i> Thông tin khách hàng
			</div>
		</div>
		<div class="portlet-body form">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-4">
						<?= $form->field($model, 'phone')->widget(Select2::className(), [
							'data'          => ArrayHelper::map(Customer::find()->asArray()->all(), 'phone', 'phone'),
							'options'       => [
								'id'                 => 'orderform-phone',
								'placeholder'        => 'SĐT',
								'allowClear'         => true,
								'minimumInputLength' => 2,
							],
							'pluginOptions' => [
								'tags' => true,
							],
						]) ?>					</div>
					<div class="col-sm-4">

						<?= $form->field($model, 'customer_name')->textInput() ?>
					</div>
					<div class="col-sm-4">

						<?= $form->field($model, 'customer_type')->dropDownList(Model::CUSTOMER_TYPE) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-3"><?= $form->field($model, 'city')->dropDownList(OrderController::getShippingCity(), [
							'id'     => 'city-id',
							'prompt' => 'Select...',
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'district')->widget(DepDrop::classname(), [
							'options'       => ['id' => 'district-id'],
							'data'          => $dataDistrict,
							'pluginOptions' => [
								'depends'     => ['city-id'],
								'placeholder' => 'Select...',
								'url'         => Url::to(['/order/get-district']),
							],
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'village')->widget(DepDrop::classname(), [
							'options'       => ['id' => 'village-id'],
							'data'          => $dataVillage,
							'pluginOptions' => [
								'depends'     => ['district-id'],
								'placeholder' => 'Select...',
								'url'         => Url::to(['/order/get-village']),
							],
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'shipping_address')->textInput() ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-4"><?= $form->field($model, 'customer_note')->widget(Redactor::className(), [
							'clientOptions' => [
								'placeholder' => 'Ghi chú khách hàng',
							],
						]) ?></div>
					<div class="col-sm-4"><?= $form->field($model, 'note')->widget(Redactor::className(), [
							'clientOptions' => [
								'placeholder' => 'Ghi chú đơn hàng',
							],
						]) ?></div>
					<div class="col-sm-4"><?= $form->field($model, 'source_note')->widget(Redactor::className(), [
							'clientOptions' => [
								'placeholder' => 'Nguồn khách',
							],
						]) ?></div>

				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 ">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="portlet box blue clearfix">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i> Thông tin cơ bản
					</div>
				</div>
				<div class="portlet-body form">
					<div class="col-sm-12">
						<?= $form->field($model, 'code')->textInput() ?>
						<?= $form->field($model, 'staff_name')->textInput() ?>
						<?= $form->field($model, 'status')->dropDownList(\app\models\Order::STATUS) ?>

					</div>
				</div>
			</div>

		</div>
		<div class="col-md-6 ">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="portlet box blue clearfix">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i> Thông tin chuyển phát
					</div>
				</div>
				<div class="portlet-body form">
					<div class="col-sm-12">
						<?= $form->field($model, 'shipping_code')->textInput() ?>
						<?= $form->field($model, 'shipping_fee')->textInput() ?>
						<?= $form->field($model, 'collect_behalf')->textInput() ?>
						<?= $form->field($model, 'service')->dropDownList(\app\models\Order::ORDER_SERVICE) ?>
						<?= $form->field($model, 'other_service')->textInput() ?>
						<div class="col-sm-6">
							<?= $form->field($model, 'is_customer_fee')->checkbox() ?>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'is_customer_check')->checkbox() ?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>
	<div class="portlet box blue clearfix">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i> Danh sách sản phẩm
			</div>
		</div>
		<div class="portlet-body form">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				//		'filterModel' => $searchModel,
				'columns'      => [
					['class' => 'yii\grid\SerialColumn'],
					'order_id',
					'product_id',
					'quantity',
					'total_price',
					//'status',
					'discount',
					//			['class' => 'yii\grid\ActionColumn'],
				],
			]); ?>
			<div class="clearfix" style="clear: both"></div>

		</div>
	</div>

</div>
