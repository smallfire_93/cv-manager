<?php
use app\models\Order;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a('Lên đơn', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<!--	--><?php echo $this->render('_search', ['model'       => $searchModel,
	                                                'orderType'   => $orderType,
	                                                'numberOrder' => $numberOrder,
	]); ?>


	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'pjax'         => false,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'code',
			'shipping_code',
			[
				'attribute' => 'customer_phone',
				'value'     => function ($data) {
					return Html::a($data->customer->phone, Url::to([
						'customer/update',
						'id' => $data->customer->id,
					]));
				},
				'format'    => 'raw',
			],
			[
				'attribute' => 'customer_id',
				'value'     => function ($data) {
					return Html::a($data->customer->name, Url::to([
						'customer/update',
						'id' => $data->customer->id,
					]));
				},
				'format'    => 'raw',
			],
			[
				'attribute' => 'total_amount_real',
				'value'     => function ($data) {
					return number_format($data->total_amount_real);
				},
			],
			'discount',
			[
				'attribute' => 'status',
				'value'     => function (Order $data) {
					return $data::STATUS[$data->status];
				},
				'filter'    => $searchModel::STATUS,
			],
			[
				'attribute' => 'customer_type',
				'value'     => function (Order $data) {
					return $data::CUSTOMER_TYPE[$data->customer->customer_type];
				},
				'filter'    => $searchModel::CUSTOMER_TYPE,
			],
			[
				'attribute' => 'note',
				'format'      => 'raw',
			],
			'created_date',
			[
				'attribute' => 'created_by',
				'value'     => function (Order $data) {
					return $data->userCreate->username ?? '';
				},
			],
			//'update_by',
			[
				'class'    => 'yii\grid\ActionColumn',
				'template' => '{update} {delete}',
			],
		],
	]); ?>
</div>
