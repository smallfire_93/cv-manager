<?php
use app\components\Model;
use app\controllers\OrderController;
use app\models\Category;
use app\models\Customer;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
Yii::$app->view->registerJs('var getInfoUrl = "' . Url::to(['customer/get-info']) . '"', \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var orderItemUrl = "' . Url::to([
		'/order/order-item',
	]) . '"', \yii\web\View::POS_HEAD);
$this->registerJsFile('@web/js/order.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="order-form">

	<?php $form = ActiveForm::begin(); ?>
	<div class="portlet box blue clearfix">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i> Thông tin khách hàng
			</div>
		</div>
		<div class="portlet-body form">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-4">
						<?= $form->field($model, 'phone')->widget(Select2::className(), [
							'data'          => ArrayHelper::map(Customer::find()->asArray()->all(), 'phone', 'phone'),
							'options'       => [
								'id'                 => 'orderform-phone',
								'placeholder'        => 'SĐT',
								'allowClear'         => true,
								'minimumInputLength' => 2,
							],
							'pluginOptions' => [
								'tags' => true,
							],
						]) ?>
					</div>
					<div class="col-sm-4">

						<?= $form->field($model, 'customer_name')->textInput() ?>
					</div>
					<div class="col-sm-4">

						<?= $form->field($model, 'customer_type')->dropDownList(Model::CUSTOMER_TYPE) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-3"><?= $form->field($model, 'city')->dropDownList(OrderController::getShippingCity(), [
							'id'     => 'city-id',
							'prompt' => 'Select...',
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'district')->widget(DepDrop::classname(), [
							'options'       => ['id' => 'district-id'],
							'pluginOptions' => [
								'depends'     => ['city-id'],
								'placeholder' => 'Select...',
								'url'         => Url::to(['/order/get-district']),
							],
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'village')->widget(DepDrop::classname(), [
							'options'       => ['id' => 'village-id'],
							'pluginOptions' => [
								'depends'     => ['district-id'],
								'placeholder' => 'Select...',
								'url'         => Url::to(['/order/get-village']),
							],
						]); ?></div>
					<div class="col-sm-3"><?= $form->field($model, 'shipping_address')->textInput() ?></div>

				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-4"><?= $form->field($model, 'customer_note')->widget(Redactor::className(), [
							'clientOptions' => [
								'placeholder' => 'Ghi chú khách hàng',
							],
						]) ?></div>
					<div class="col-sm-4"><?= $form->field($model, 'note')->widget(Redactor::className(), [
							'clientOptions' => [
								'placeholder' => 'Ghi chú đơn hàng',
							],
						]) ?></div>
					<div class="col-sm-4"><?= $form->field($model, 'source_note')->widget(Redactor::className(), [
							'clientOptions' => [
								'placeholder' => 'Nguồn khách',
							],
						]) ?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="portlet box yellow clearfix">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i> Chọn sản phẩm
			</div>
		</div>
		<div class="portlet-body form">
			<div class="item-header">
				<div class="col-sm-1 id grid-display"><p style="text-transform: uppercase">STT</p></div>
				<div class="col-sm-3 category-select grid-display">
					<p style="text-transform: uppercase">Tên danh mục</p>
				</div>
				<div class="product-select grid-display col-sm-3"><p style="text-transform: uppercase">Tên sản phẩm</p>
				</div>
				<div class="quantity grid-display col-sm-1"><p style="text-transform: uppercase">Số lượng</p></div>
				<div class="discount grid-display col-sm-2"><p style="text-transform: uppercase">Giảm giá(VNĐ)</p>
				</div>
				<div class="price-show grid-display col-sm-2"><p style="text-transform: uppercase">Tổng tiền</p>
				</div>
			</div>
			<div class="items">
				<?php for(
					$i = 1; $i < 2; $i ++
				) { ?>
					<div class="item-detail">
						<div class="col-sm-1 id grid-display"><?= Html::input('text', '', $i, [
								'class'    => 'ordinal form-control form-height form-boder',
								"disabled" => true,
							]) ?></div>
						<div class="col-sm-3 category-select grid-display"><?= Html::dropDownList('', $orderItem->isNewRecord ? '' : $orderItem->product->category_id, Category::getCategoryOrder(), [
								'class'  => 'form-control form-height form-boder ',
								'prompt' => 'Chọn danh mục',
								'style'  => 'float:left',
							]) ?>
						</div>
						<div class="col-sm-3 product-select grid-display">
							<div class="overflow"><?= Html::activeDropDownList($orderItem, 'product_id', [], [
									'name'   => 'OrderItem[' . $i . '][product_id]',
									'class'  => 'form-control form-height form-boder',
									'prompt' => 'Chọn sản phẩm',
									'style'  => 'float:left',
								]) ?></div>
						</div>
						<div class="col-sm-1 quantity grid-display"><?= Html::activeTextInput($orderItem, 'quantity', [
								'class' => 'form-control form-height form-boder quantity-input',
								'type'  => 'number',
								'min'   => 0,
								'name'  => 'OrderItem[' . $i . '][quantity]',
							]) ?></div>
						<div class="col-sm-2 discount grid-display"><?= Html::activeTextInput($orderItem, 'discount', [
								'class' => 'form-control form-height form-boder',
								'name'  => 'OrderItem[' . $i . '][discount]',
								'value' => 0,
							]) ?></div>
						<div class="col-sm-2 price-show grid-display"><?= Html::activeTextInput($orderItem, 'total_price', [
								'class'    => 'form-control form-height form-boder',
								'disabled' => true,
							]) ?></div>
					</div>
				<?php } ?>
			</div>
			<div class="clearfix" style="clear: both"></div>

			<div class="row action-pager" style="margin: 10px 0; margin-left: -5px">
				<div class="col-sm-6 action-item add-item">
					<a class="fleft add-form btn btn-info" href="">Thêm sản phẩm</a>
				</div>
			</div>

			<div class=" row final-total">

				<div class="col-sm-6 total">
					<p></p>
				</div>
				<div class="detail-total col-sm-6 ">
					<div class="col-sm-6 label-item">
						<p>Tổng giá trị đơn hàng:</p>
						<!--			<p>Giảm giá: </p>-->
						<!--			<p>Tổng số:</p>-->
					</div>
					<div class="col-sm-6 value-item">
						<p>0</p>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

<script>
</script>