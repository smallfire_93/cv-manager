<?php
use app\models\Order;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\search\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

	<div class="tabbable-line" style="padding: 15px 0">
		<ul class="order-nav nav nav-tabs " style="display: flex">
			<?php foreach(\yii\helpers\ArrayHelper::merge([0 => 'Tất cả'], Order::STATUS) as $key => $status) { ?>
				<li class="<?= $key == $orderType ? 'active' : '' ?>">
					<?php echo Html::a($status.' (<span style="color: #F3565D">'.$numberOrder[$key].'</span>)', [
						'/order',
						'orderType' => $key,
					]) ?>
				</li>
			<?php } ?>
		</ul>
	</div>

	<?php $form = ActiveForm::begin([
		'action' => [
			'index',
			'orderType' => $orderType,
		],
		'method' => 'get',
	]); ?>


	<?php // echo $form->field($model, 'code') ?>

	<?php // echo $form->field($model, 'created_date') ?>

	<?php // echo $form->field($model, 'update_at') ?>
	<div class="row">

	</div>
	<div class="row">
		<div class="col-sm-6">

			<?php echo $form->field($model, 'phone_code')->textInput(['placeholder' => 'Tìm theo sđt, mã đơn hàng, mã vận đơn'])->label(false) ?>
		</div>
		<div class="col-sm-4">

			<?php echo $form->field($model, 'createTimeRange', [
				'addon'   => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
				'options' => ['class' => 'drp-container form-group'],
			])->widget(\kartik\daterange\DateRangePicker::className(), [
				'useWithAddon'  => true,
				'convertFormat' => true,
				'presetDropdown'=>true,
//				'value' => '2016-09-05 - 2017-09-05',
				'pluginOptions' => [
					'format' => 'YYYY-MM-DD',
				],
			])->label(false) ?>
		</div>
		<div class="form-group pull-left">
			<?= Html::submitButton('Tìm kiếm', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>

</div>
