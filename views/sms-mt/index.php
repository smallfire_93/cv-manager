<?php
use app\models\SmsMt;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SmsMtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Sms Mts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-mt-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'phone',
			'content:ntext',
			'created_at',
			'candidate_name',
			'status',
			[
				'attribute' => 'type',
				'value'     => function (SmsMt $data) {
					if(isset($data->type)) {
						return $data::TYPE[$data->type];
					} else {
						return 'old sms';
					}
				},
			],
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
</div>
