<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmsMt */

$this->title = 'Create Sms Mt';
$this->params['breadcrumbs'][] = ['label' => 'Sms Mts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-mt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
