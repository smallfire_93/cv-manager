<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrderItem;

/**
 * OrderItemSearch represents the model behind the search form of `app\models\OrderItem`.
 */
class OrderItemSearch extends OrderItem {

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'order_id',
					'product_id',
					'quantity',
					'status',
				],
				'integer',
			],
			[
				[
					'total_price',
					'discount',
				],
				'number',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $id) {
		$query = OrderItem::find();
		if($id) {
			$query->where(['order_id' => $id]);
		}
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'          => $this->id,
			'product_id'  => $this->product_id,
			'quantity'    => $this->quantity,
			'total_price' => $this->total_price,
			'status'      => $this->status,
			'discount'    => $this->discount,
		]);
		return $dataProvider;
	}
}
