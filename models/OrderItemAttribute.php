<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_item_attribute".
 *
 * @property int $id
 * @property int $order_item_id
 * @property int $attribute_value_id
 * @property int $product_id
 */
class OrderItemAttribute extends \app\components\Model
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_item_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_item_id', 'attribute_value_id', 'product_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_item_id' => 'Order Item ID',
            'attribute_value_id' => 'Attribute Value ID',
            'product_id' => 'Product ID',
        ];
    }
}
