<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 8/1/2018
 * Time: 7:37 PM
 */

namespace app\models;

use yii\base\Model;

class OrderForm extends Model {

	public $staff_name;

	public $code;

	public $discount;

	public $customer_name;

	public $customer_type;

	public $phone;

	public $address;

	public $shipping_address;

	public $shipping_fee;

	public $status;

	public $note;

	public $customer_note;

	public $created_date;

	public $city;

	public $district;

	public $village;

	public $product;

	public $weight;

	public $product_content;

	public $is_customer_fee;

	public $collect_behalf;

	public $service;

	public $other_service;

	public $is_customer_check;

	public $shipping_code;

	public $total_amount;

	public $total_amount_real;

	public $source_note;

	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			[
				[
					'customer_name',
					'phone',
				],
				'required',
			],
			[
				[
					'city',
					'status',
					'district',
					'village',
					'product',
					'is_customer_check',
					'is_customer_fee',
					'customer_type',
				],
				'integer',
			],
			[
				[
					'discount',
					'shipping_fee',
					'total_amount',
					'total_amount_real',
					'collect_behalf',
				],
				'number',
			],
			[
				[
					'phone',
					'staff_name',
					'address',
					'shipping_address',
					'customer_name',
					'code',
					'note',
					'created_date',
					'service',
					'other_service',
					'shipping_code',
					'customer_note',
					'source_note'
				],
				'string',
			],
		];
	}

	public function attributeLabels() {
		return [
			'id'                => 'ID',
			'customer_name'     => 'Tên người mua',
			'customer_type'     => 'Phân loại khách hàng',
			'source_note'       => 'Nguồn khách',
			'city'              => 'Thành phố giao hàng',
			'code'              => 'Mã đơn',
			'district'          => 'Quận/huyện giao hàng',
			'phone'             => 'Số điện thoại',
			'staff_name'        => 'Tên nhân viên',
			'address'           => 'Địa chỉ',
			'shipping_address'  => 'Địa chỉ chi tiết',
			'note'              => 'Ghi chú đơn hàng',
			'customer_note'     => 'Ghi chú khách hàng',
			'created_date'      => 'Ngày mua',
			'status'            => 'Trạng thái',
			'product'           => 'Chọn sản phẩm',
			'weight'            => 'Trọng lượng',
			'product_content'   => 'Nội dung hàng hóa',
			'is_customer_fee'   => 'Người nhận trả trước',
			'collect_behalf'    => 'Số tiền thu hộ',
			'service'           => 'Dịch vụ',
			'other_service'     => 'Dịch vụ khác',
			'is_customer_check' => 'Cho khách xem hàng',
			'shipping_code'     => 'Mã vận đơn',
		];
	}
}