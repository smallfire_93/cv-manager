<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int    $id
 * @property int    $parent_id
 * @property int    $type
 * @property int    $sort_order
 * @property string $image
 * @property string $name
 */
class Category extends \app\components\Model {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'category';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'parent_id',
					'type',
					'sort_order',
				],
				'integer',
			],
			[
				[
					'image',
					'name',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'parent_id'  => 'Danh mục cha',
			'type'       => 'Type',
			'sort_order' => 'Sort Order',
			'image'      => 'Image',
			'name'       => 'Tên danh mục',
		];
	}
}
