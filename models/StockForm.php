<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class StockForm extends Model {

	public $name;

	public $quantity;

	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			[
				[
					'quantity',
				],
				'required',
			],
			[
				'name',
				'safe',
			],
		];
	}

	public function attributeLabels() {
		return [
			'name'     => 'Tên sản phẩm',
			'quantity' => 'Số lượng thêm vào kho',
		];
	}
}
