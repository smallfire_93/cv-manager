<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_mt".
 *
 * @property string $id
 * @property string $phone
 * @property string $content
 * @property string $created_at
 * @property int    $cv_id
 * @property string $candidate_name
 * @property int    $status
 */
class SmsMt extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'sms_mt';
	}

	const TYPE       = [
		1 => 'First sms',
		2 => 'Repeat sms',
	];

	const FIRST_SMS  = 1;

	const REPEAT_SMS = 2;

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'phone',
					'cv_id',
					'status',
				],
				'required',
			],
			[
				['content'],
				'string',
			],
			[
				['created_at'],
				'safe',
			],
			[
				[
					'cv_id',
					'status',
					'type',
				],
				'integer',
			],
			[
				[
					'phone',
					'candidate_name',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'             => 'ID',
			'phone'          => 'Số điện thoại',
			'content'        => 'Nội dung',
			'created_at'     => 'Ngày gửi',
			'cv_id'          => 'Cv ID',
			'candidate_name' => 'Tên ứng viên',
			'status'         => 'Trạng thái',
		];
	}
}
