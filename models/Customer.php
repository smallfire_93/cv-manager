<?php

namespace app\models;

use navatech\role\models\User;
use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int    $id
 * @property string $name
 * @property string $address
 * @property string $shipping_address
 * @property string $note
 * @property string $email
 * @property string $phone
 * @property string $created_date
 * @property string $update_date
 * @property int    $city_id
 * @property int    $shipping_city_id
 * @property int    $shipping_district_id
 * @property int    $shipping_village_id
 * @property int    $user_id
 * @property int    $status
 * @property float  $total_money
 * @property Order  $order
 */
class Customer extends \app\components\Model {

	/**
	 * {@inheritdoc}
	 */
	const STATUS     = [
		'Chưa mua hàng',
		'Đã từng mua hàng',
	];

	const STATUS_BUY = 1;

	const STATUS_NOT = 0;

	public $shipping_code;

	public $total_money;

	public static function tableName() {
		return 'customer';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'phone',
				],
				'required',
			],
			[
				[
					'note',
					'phone',
				],
				'string',
			],
			[
				[
					'city_id',
					'shipping_city_id',
					'shipping_district_id',
					'user_id',
					'status',
					'shipping_village_id',
					'customer_type',
				],
				'integer',
			],
			[
				[
					'created_date',
					'update_date',
				],
				'safe',
			],
			[
				[
					'name',
					'address',
					'shipping_address',
					'email',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'                   => 'ID',
			'name'                 => 'Họ tên',
			'address'              => 'Địa chỉ',
			'shipping_address'     => 'Địa chỉ mua hàng',
			'note'                 => 'Note',
			'email'                => 'Email',
			'phone'                => 'Số điện thoại',
			'city_id'              => 'Thành phố',
			'shipping_city_id'     => 'Thành phố giao hàng',
			'shipping_district_id' => 'Huyện giao hàng',
			'shipping_village_id'  => 'Phường/xã giao hàng',
			'user_id'              => 'Người tạo',
			'status'               => 'Trạng thái',
			'shipping_code'        => 'Mã vận đơn',
			'total_money'          => 'Tổng tiền đơn hàng',
		];
	}

	public function getDistrict() {
		return $this->hasOne(Province::className(), ['id' => 'shipping_district_id']);
	}

	public function getVillage() {
		return $this->hasOne(Province::className(), ['id' => 'shipping_village_id']);
	}

	public function getCity() {
		return $this->hasOne(Province::className(), ['id' => 'shipping_city_id']);
	}

	public function getUserAdmin() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getOrders() {
		return $this->hasMany(Order::className(), ['customer_id' => 'id']);
	}

	public function getAllOrderCode() {
		$orders = $this->orders;
		$code   = '';
		foreach($orders as $order) {
			$code .= $order->shipping_code ? "<p>" . $order->shipping_code . "</p>" : '';
		}
		return $code;
	}

	public function getAllOrderMoney() {
		$orders = $this->orders;
		$code   = 0;
		foreach($orders as $order) {
			$code += $order->total_amount_real;
		}
		return number_format($code);
	}
}
