<?php

namespace app\models;

use app\components\Model;
use navatech\role\models\User;
use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int      $id
 * @property int      $user_id
 * @property double   $total_amount
 * @property double   $total_amount_real
 * @property double   $discount
 * @property string   $note
 * @property string   $created_date
 * @property string   $update_at
 * @property int      $status
 * @property int      $update_by
 * @property int      $customer_id
 * @property int      $created_by
 * @property string   $staff_name
 * @property double   $shipping_fee
 * @property double   $weight
 * @property string   $product_content
 * @property int      $is_customer_fee
 * @property double   $collect_behalf
 * @property string   $service
 * @property string   $other_service
 * @property int      $is_customer_check
 * @property int      $is_issue
 * @property string   $shipping_code
 * @property string   $code
 * @property Customer $customer
 */
class Order extends Model {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'order';
	}

	public $customer_phone;

	public $phone_code;

	const STATUS           = [
		1  => 'Lên đơn thành công',
		2  => 'Đang vận chuyển',
		3  => 'Giao thành công',
		4  => 'Hàng đang hoàn',
		5  => 'Hoàn thành công',
		6  => 'Mất hàng / giao chậm',
		7  => 'Không liên hệ được',
		8  => 'Duyệt hoàn',
		9  => 'Đang đi phát hàng',
		99 => 'Đơn hàng đã hủy',
	];

	const STATUS_CUSTOMER  = [
		'Khách chuẩn',
		'Khách vớ vẩn',
		'Khách bom hàng',
		'Không có tiền',
	];

	const ISSUED           = 1;

	const NOT_ISSUE        = 0;

	const STATUS_PENDING   = 1;

	const STATUS_SHIP      = 2;

	const STATUS_SUCCESS   = 3;

	const STATUS_RETURNING = 4;

	const STATUS_RETURNED  = 5;

	const STATUS_LOST      = 6;

	const STATUS_CANCEL    = 99;

	const ORDER_SERVICE    = [
		'SCOD' => 'SCOD',
		'PHS'  => 'PHS',
		'PTN'  => 'PTN',
		'PHT'  => 'PHT',
		'VCN'  => 'VCN',
		'VTK'  => 'VTK',
		'VHT'  => 'VHT',
		'V60'  => 'V60',
		'V36'  => 'V36',
		'VVT'  => 'VVT',
		'VBS'  => 'VBS',
		'VBE'  => 'VBE',
	];

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'status',
					'update_by',
					'customer_id',
					'created_by',
					'is_customer_fee',
					'is_customer_check',
					'is_issue',
				],
				'integer',
			],
			[
				[
					'total_amount',
					'total_amount_real',
					'status',
				],
				'required',
			],
			[
				[
					'total_amount',
					'total_amount_real',
					'discount',
					'shipping_fee',
					'weight',
					'collect_behalf',
				],
				'number',
			],
			[
				[
					'created_date',
					'update_at',
					'customer',
					'customer_type',
					'source_note',
				],
				'safe',
			],
			[
				[
					'staff_name',
					'product_content',
					'service',
					'other_service',
					'shipping_code',
					'code',
				],
				'string',
				'max' => 255,
			],
			[
				[
					'note',
					'source_note',
				],
                'string',
                'max' => 1000,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'                => 'ID',
			'user_id'           => 'Tạo bởi',
			'total_amount'      => 'Total Amount',
			'total_amount_real' => 'Tổng tiền',
			'source_note'       => 'Nguồn',
			'discount'          => 'Discount',
			'note'              => 'Note',
			'created_date'      => 'Ngày tạo',
			'update_at'         => 'Update At',
			'status'            => 'Trạng thái',
			'update_by'         => 'Update By',
			'customer_id'       => 'Tên Khách hàng',
			'created_by'        => 'Tạo bởi',
			'staff_name'        => 'Staff Name',
			'shipping_fee'      => 'Shipping Fee',
			'weight'            => 'Weight',
			'product_content'   => 'Product Content',
			'is_customer_fee'   => 'Is Customer Fee',
			'collect_behalf'    => 'Collect Behalf',
			'service'           => 'Service',
			'other_service'     => 'Other Service',
			'is_customer_check' => 'Is Customer Check',
			'shipping_code'     => 'Mã vận đơn',
			'code'              => 'Mã đơn',
			'customer_phone'    => 'Sđt khách hàng',
			'customer_type'     => 'Loại khách',
		];
	}

	public function getCustomer() {
		return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}

	public function getUserCreate() {
		return $this->hasOne(User::className(), ['id' => 'created_by']);
	}
}
