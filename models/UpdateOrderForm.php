<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/10/2019
 * Time: 3:57 PM
 */

namespace app\models;
use app\components\Form;

class UpdateOrderForm extends Form {
	public $staff_name;

	public $code;

	public $discount;

	public $customer_name;

	public $phone;

	public $address;

	public $shipping_address;

	public $shipping_fee;

	public $status;

	public $note;

	public $created_date;

	public $city;

	public $district;

	public $product;

	public $weight;

	public $product_content;

	public $is_customer_fee;

	public $collect_behalf;

	public $service;

	public $other_service;

	public $is_customer_check;

	public $shipping_code;

	public $total_amount;

	public $total_amount_real;

	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			[
				[
					'customer_name',
					'phone',
				],
				'required',
			],
			[
				[
					'city',
					'status',
					'district',
					'product',
				],
				'integer',
			],
			[
				[
					'discount',
					'shipping_fee',
					'total_amount',
					'total_amount_real',
				],
				'number',
			],
			[
				[
					'phone',
					'staff_name',
					'address',
					'shipping_address',
					'customer_name',
					'code',
					'note',
					'created_date',
				],
				'string',
			],
		];
	}

	public function attributeLabels() {
		return [
			'id'                => 'ID',
			'customer_name'     => 'Người mua',
			'city'              => 'Thành phố giao hàng',
			'code'              => 'Mã đơn',
			'district'          => 'Quận/huyện giao hàng',
			'phone'             => 'Số điện thoại',
			'staff_name'        => 'Tên nhân viên',
			'address'           => 'Địa chỉ',
			'shipping_address'  => 'Địa chỉ chi tiết',
			'note'              => 'Ghi chú',
			'created_date'      => 'Ngày mua',
			'status'            => 'Trạng thái',
			'product'           => 'Chọn sản phẩm',
			'weight'            => 'Trọng lượng',
			'product_content'   => 'Nội dung hàng hóa',
			'is_customer_fee'   => 'Người nhận trả trước',
			'collect_behalf'    => 'Số tiền thu hộ',
			'service'           => 'Dịch vụ',
			'other_service'     => 'Dịch vụ khác',
			'is_customer_check' => 'Cho khách xem hàng',
			'shipping_code'     => 'Mã vận đơn',
		];
	}
}