<?php

namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int    $id
 * @property int    $category_id
 * @property string $name
 * @property string $code
 * @property string $image
 * @property string $product_img
 * @property int    $in_stock
 * @property double $base_price
 * @property double $weight
 * @property double $price
 * @property string $description
 * @property string $created_date
 */
class Product extends Model {

	public $product_img;

	public $add_stock;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'product';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'category_id',
					'name',
					'base_price',
					'price',
					'weight',
				],
				'required',
			],
			[
				[
					'category_id',
					'in_stock',
				],
				'integer',
			],
			[
				[
					'weight',
					'base_price',
					'price',
				],
				'safe',
			],
			[
				['description'],
				'string',
			],
			[
				['created_date'],
				'safe',
			],
			[
				[
					'name',
					'code',
					'image',
				],
				'string',
				'max' => 255,
			],
			[
				['product_img'],
				'file',
				'extensions' => 'jpg, gif, png',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'          => 'ID',
			'category_id' => 'Danh mục',
			'name'        => 'Tên sản phẩm',
			'code'        => 'Mã sản phẩm',
			'product_img' => 'Ảnh sản phẩm',
			'in_stock'    => 'Số lượng nhập kho',
			'base_price'  => 'Giá nhập',
			'price'       => 'Giá bán',
			'description' => 'Mô tả',
			'add_stock'   => 'Nhập kho',
			'weight'      => 'Cân nặng',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory() {
		return $this->hasOne(Category::className(), ['id' => 'category_id']);
	}
}
