<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stock_log".
 *
 * @property int    $id
 * @property int    $product_id
 * @property int    $quantity
 * @property double $total_price
 * @property double $base_price
 * @property int    $type
 * @property int    $order_id
 */
class StockLog extends \app\components\Model {

	const TYPE         = [
		1 => 'Nhập hàng',
		2 => 'Xuất kho',
		3 => 'Hàng hoàn',
	];

	const TYPE_RECEIPT = 1;

	const TYPE_ISSUE   = 2;

	const TYPE_RETURN  = 3;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'stock_log';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'product_id',
					'quantity',
				],
				'required',
			],
			[
				[
					'product_id',
					'quantity',
					'type',
					'order_id',
				],
				'integer',
			],
			[
				[
					'total_price',
					'base_price',
				],
				'number',
			],
			[
				'created_at',
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'product_id'   => 'Sản phẩm',
			'quantity'     => 'Số lượng',
			'total_price'  => 'Tổng tiền xuất/nhập',
			'base_price'   => 'Tổng gốc',
			'type'         => 'Kiểu',
			'order_id'     => 'Đơn hàng',
			'created_date' => 'Thời gian',
		];
	}

	public function getProduct() {
		return $this->hasOne(Product::className(), ['id' => 'product_id']);
	}

	public function getOrders() {
		return $this->hasOne(Order::className(), ['id' => 'order_id']);
	}
}
