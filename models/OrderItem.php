<?php

namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "order_item".
 *
 * @property int    $id
 * @property int    $order_id
 * @property int    $product_id
 * @property int    $quantity
 * @property int    $status
 * @property double $total_price
 * @property double $discount
 */
class OrderItem extends Model {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'order_item';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'order_id',
					'product_id',
					'quantity',
					'total_price',
				],
				'required',
			],
			[
				[
					'order_id',
					'product_id',
					'quantity',
					'status',
				],
				'integer',
			],
			[
				[
					'total_price',
					'discount',
				],
				'number',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'          => 'ID',
			'order_id'    => 'Order ID',
			'product_id'  => 'Product ID',
			'quantity'    => 'Quantity',
			'total_price' => 'Total Price',
		];
	}
}
