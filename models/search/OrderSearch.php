<?php

namespace app\models\search;

use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form of `app\models\Order`.
 */
class OrderSearch extends Order {

	public $customer_phone;

	public $customer_type;

	public $createTimeRange;

	public $createTimeStart;

	public $createTimeEnd;

	public function behaviors() {
		return [
			[
				'class'              => DateRangeBehavior::className(),
				'attribute'          => 'createTimeRange',
				'dateStartAttribute' => 'createTimeStart',
				'dateEndAttribute'   => 'createTimeEnd',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id',
					'status',
					'update_by',
					'customer_id',
				],
				'integer',
			],
			[
				[
					'total_amount',
					'total_amount_real',
					'discount',
				],
				'number',
			],
			[
				[
					'note',
					'created_date',
					'update_at',
					'customer_phone',
					'phone_code',
					'customer_type',
					'createTimeRange',
					'source_note',
				],
				'safe',
			],
			[
				['createTimeRange'],
				'match',
				'pattern' => '/^.+\s\-\s.+$/',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @param       $orderType
	 * @param bool  $disablePagination
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $orderType, $disablePagination = false) {
		$query = Order::find()->joinWith('customer');
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		if ($disablePagination) {
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
				'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
				'pagination'  => false,
			]);
		}
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'                     => $this->id,
			'order.user_id'          => $this->user_id,
			'total_amount'           => $this->total_amount,
			'total_amount_real'      => $this->total_amount_real,
			'discount'               => $this->discount,
			'code'                   => $this->code,
			'shipping_code'          => $this->shipping_code,
			'update_at'              => $this->update_at,
			'order.status'           => $this->status,
			'order.update_by'        => $this->update_by,
			'customer_id'            => $this->customer_id,
			'customer.customer_type' => $this->customer_type,
		]);
		if($orderType != 0) {
			$query->andFilterWhere([
				'order.status' => $orderType,
			]);
		}
		$query->andFilterWhere([
			'like',
			'customer.phone',
			$this->phone_code,
		])->orFilterWhere([
			'like',
			'order.code',
			$this->phone_code,
		])->orFilterWhere([
			'like',
			'order.shipping_code',
			$this->phone_code,
		]);
		if(!$this->createTimeStart && !$this->createTimeEnd) {

			$this->createTimeStart = strtotime(date('Y-m-d 00:00:01', strtotime('-30 days')));
			$this->createTimeEnd   = strtotime(date('Y-m-d'));

			$this->createTimeRange = date('Y-m-d', strtotime('-30 days')) .' - '. date('Y-m-d') ;
		}
		$query->andFilterWhere([
			'>=',
			'order.created_date',
			date('Y-m-d H:i:s', $this->createTimeStart),
		]);
		$query->andFilterWhere([
			'<=',
			'order.created_date',
			date('Y-m-d H:i:s', strtotime("tomorrow", $this->createTimeEnd) - 1),
		]);
		$query->andFilterWhere([
			'like',
			'note',
			$this->note,
		]);
		$query->andFilterWhere([
			'like',
			'source_note',
			$this->source_note,
		]);
		$query->andFilterWhere([
			'like',
			'customer.phone',
			$this->customer_phone,
		]);
		return $dataProvider;
	}
}
