<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CvInfo;

/**
 * CvInfoSearch represents the model behind the search form of `app\models\CvInfo`.
 */
class CvInfoSearch extends CvInfo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'gender', 'trainer_id', 'city', 'id_number', 'user_id'], 'integer'],
            [['name', 'birthday', 'phone', 'email', 'cv', 'address', 'interview_day', 'trial_start', 'trial_end', 'official_start', 'trainer_name', 'hometown', 'password', 'note', 'picture'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CvInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'status' => $this->status,
            'gender' => $this->gender,
            'interview_day' => $this->interview_day,
            'trial_start' => $this->trial_start,
            'trial_end' => $this->trial_end,
            'official_start' => $this->official_start,
            'trainer_id' => $this->trainer_id,
            'city' => $this->city,
            'id_number' => $this->id_number,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'cv', $this->cv])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'trainer_name', $this->trainer_name])
            ->andFilterWhere(['like', 'hometown', $this->hometown])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'picture', $this->picture]);

        return $dataProvider;
    }
}
