<?php

namespace app\models;

use app\components\Model;
use phpDocumentor\Reflection\Types\Self_;
use Yii;

/**
 * This is the model class for table "cv_info".
 *
 * @property int    $id
 * @property string $name
 * @property string $birthday
 * @property string $phone
 * @property string $email
 * @property string $cv
 * @property string $address
 * @property string $apply_position
 * @property string $current_position
 * @property int    $status
 * @property int    $gender
 * @property string $interview_day
 * @property string $trial_start
 * @property string $trial_end
 * @property string $official_start
 * @property int    $trainer_id
 * @property string $trainer_name
 * @property int    $city
 * @property string $hometown
 * @property int    $id_number
 * @property int    $user_id
 * @property string $password
 * @property int    $is_sms
 * @property string $note
 * @property string $picture
 * @property string $cv_file
 */
class CvInfo extends Model {

	/**
	 * {@inheritdoc}
	 */
	public $cv_file;

	public static function tableName() {
		return 'cv_info';
	}

	const IS_SMS           = [
		'Chưa gửi',
		'Đã gửi lần 1',
		'Đã nhắc nhở',
	];

	const NO_SMS           = 0;

	const FIRST_SMS        = 1;

	const REPEAT_SMS       = 2;

	const STATUS           = [
		'Trượt',
		'Đỗ',
		'Chờ phỏng vấn',
	];

	const GENDER           = [
		'Nam',
		'Nữ',
	];

	const STATUS_FAILED    = 0;

	const STATUS_PASS      = 1;

	const STATUS_INTERVIEW = 2;

	const NAME             = '{NAME}';

	const PHONE            = '{PHONE}';

	const DATE             = '{DATE}';

	const HOUR             = '{HOUR}';

	const POSITION         = '{POSITION}';

	const TRAINER_NAME     = '{TRAINER_NAME}';

	const DAY              = '{DAY}';

	const MONTH            = '{MONTH}';

	const YEAR             = '{YEAR}';

	const DAY_ARRAY        = [
		'Mon' => 'Thu 2',
		'Tue' => 'Thu 3',
		'Wed' => 'Thu 4',
		'Thu' => 'Thu 5',
		'Fri' => 'Thu 6',
		'Sat' => 'Thu 7',
		'Sun' => 'Chu nhat',
	];

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'birthday',
					'interview_day',
					'trial_start',
					'trial_end',
					'official_start',
				],
				'safe',
			],
			[
				[
					'phone',
					'email',
					'name',
					'status',
					'interview_day',
				],
				'required',
			],
			[
				[
					'status',
					'gender',
					'trainer_id',
					'city',
					'id_number',
					'user_id',
					'is_sms',
				],
				'integer',
			],
			[
				['note'],
				'string',
			],
			[
				[
					'name',
					'phone',
					'email',
					'cv',
					'address',
					'trainer_name',
					'hometown',
					'picture',
					'apply_position',
					'current_position',
				],
				'string',
				'max' => 255,
			],
			[
				['password'],
				'string',
				'max' => 1000,
			],
			[
				['cv_file'],
				'file',
				'extensions' => 'doc, docx, pdf',
			],
		];
	}

	public function cleanString($text) {
		// 1) convert á ô => a o
		$text = preg_replace("/[áàạảâãªäậấẫầặắẵằ]/u", "a", $text);
		$text = preg_replace("/[ÁÀẠẢÂÃÄẬẤẪẦẶẮẴẰẲẨ]/u", "A", $text);
		$text = preg_replace("/[ÍÌÎÏĨỊỈ]/u", "I", $text);
		$text = preg_replace("/[íìîïĩịỉ]/u", "i", $text);
		$text = preg_replace("/[éèêëẹẽẻếềệễể]/u", "e", $text);
		$text = preg_replace("/[ÉÈÊËẸẼẺẾỀỆỄỂ]/u", "E", $text);
		$text = preg_replace("/[óòôõºöộọỏốồỗổơờớỡởợ]/u", "o", $text);
		$text = preg_replace("/[ÓÒÔÕÖỌỎỐỒỘỔỖƠỜỚỢỞỠ]/u", "O", $text);
		$text = preg_replace("/[úùûüũủụưứừữửự]/u", "u", $text);
		$text = preg_replace("/[Đ]/u", "D", $text);
		$text = preg_replace("/[đ]/u", "đ", $text);
		$text = preg_replace("/[ÚÙÛÜŨỦỤƯỮỰỬỨỪ]/u", "U", $text);
		$text = preg_replace("/[’‘‹›‚]/u", "'", $text);
		$text = preg_replace("/[“”«»„]/u", '"', $text);
		$text = str_replace("–", "-", $text);
		$text = str_replace(" ", " ", $text);
		$text = str_replace("ç", "c", $text);
		$text = str_replace("Ç", "C", $text);
		$text = str_replace("ñ", "n", $text);
		$text = str_replace("Ñ", "N", $text);
		$text = preg_replace("/[^A-Za-z0-9 ]/", '', $text);
		return $text;
	}

	public function sendMessage(CvInfo $model, $raw_message) {
		$curl         = curl_init();
		$phone_number = '84' . substr($model->phone, 1);
		$message      = $this->cleanString($model->getMessage($model, $raw_message));
		curl_setopt_array($curl, array(
			CURLOPT_PORT           => "8888",
			CURLOPT_URL            => "http://115.84.179.60:8888/doitac.asmx?wsdl=",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "POST",
			/*            CURLOPT_POSTFIELDS => "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n    <soap:Body>\n        \t <DoiTac_To_Nisco xmlns=\"http://tempuri.org/\"> -->\n\t <RequestID>gfh</RequestID>\n     <UserID>string</UserID> \n      <Message>string</Message> \n     <PartnerUsername>string</PartnerUsername> \n      <PartnerPassword>string</PartnerPassword>\n\t </DoiTac_To_Nisco>\n    </soap:Body>\n</soap:Envelope>",*/
			CURLOPT_POSTFIELDS     => "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
    <soap:Body>
        	 <DoiTac_To_Nisco xmlns=\"http://tempuri.org/\"> -->
	 <RequestID>$model->id</RequestID>
     <UserID>$phone_number</UserID> 
      <Message>$message</Message> 
     <PartnerUsername>MPGCOMPANY</PartnerUsername> 
      <PartnerPassword>MPG@COMP@NY</PartnerPassword>
	 </DoiTac_To_Nisco>
    </soap:Body>
</soap:Envelope>",
			CURLOPT_HTTPHEADER     => array(
				"cache-control: no-cache",
				"content-type: text/xml",
			),
		));
		$response = curl_exec($curl);
		$err      = curl_error($curl);
		curl_close($curl);
		if($err) {
			echo "cURL Error #:" . $err;
		} else {
			$sms_mt = new SmsMt();
			if($model->is_sms == self::NO_SMS) {
				$model->updateAttributes([
					'is_sms' => self::FIRST_SMS,
					'status' => 2,
				]);
				$sms_mt->type = $sms_mt::FIRST_SMS;
			} else {
				$model->updateAttributes([
					'is_sms' => self::REPEAT_SMS,
				]);
				$sms_mt->type = $sms_mt::REPEAT_SMS;
			}
			$sms_mt->phone          = $phone_number;
			$status                 = $this->get_string_between($response, '<DoiTac_To_NiscoResult>', '</DoiTac_To_NiscoResult>');
			$sms_mt->status         = (int) $status;
			$sms_mt->candidate_name = $model->name;
			$sms_mt->cv_id          = $model->id;
			$sms_mt->content        = $message;
			$sms_mt->created_at     = date('Y-m-d H:i:s');
			$sms_mt->save();
			echo $response;
		}
	}

	public function get_string_between($string, $start, $end) {
		$string = ' ' . $string;
		$ini    = strpos($string, $start);
		if($ini == 0) {
			return '';
		}
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	public function getMessage(CvInfo $model, $raw_message) {
		$interview_day  = $model->interview_day;
		$date           = date_format(date_create($interview_day), 'd');
		$month          = date_format(date_create($interview_day), 'm');
		$year           = date_format(date_create($interview_day), 'Y');
		$day            = CvInfo::DAY_ARRAY[date('D', strtotime(date_format(date_create($interview_day), 'd-m-Y')))];
		$time           = date_format(date_create($interview_day), 'H:i');
		$position       = $model->apply_position;
		$trainer        = $model->trainer_name;
		$array_replaces = [
			$model::NAME         => $model->name,
			$model::HOUR         => $time,
			$model::DATE         => $date,
			$model::DAY          => $day,
			$model::MONTH        => $month,
			$model::YEAR         => $year,
			$model::POSITION     => $position,
			$model::TRAINER_NAME => $trainer,
		];
		$message        = $raw_message;
		foreach($array_replaces as $key => $array_replace) {
			$message = str_replace($key, $array_replace, $message);
		}
		return $message;
	}

	public static function sendEmail(CvInfo $model) {
		$content = $model->getMessage($model, Yii::$app->setting->email_template);
		Yii::$app->mailer->compose('layouts/html', ['content' => $content])->setFrom(['tuyendung@minhphonggroup.vn' => 'Minh Phong Group'])->setTo($model->email)->setSubject(Yii::$app->setting->email_title)->send();
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'               => 'ID',
			'name'             => 'Họ và tên',
			'birthday'         => 'Ngày sinh',
			'phone'            => 'Số điện thoại',
			'email'            => 'Email',
			'cv'               => 'Cv upload',
			'address'          => 'Địa chỉ',
			'status'           => 'Trạng Thái',
			'gender'           => 'Giới tính',
			'interview_day'    => 'Ngày phỏng vấn',
			'trial_start'      => 'Ngày bắt đầu thử việc',
			'trial_end'        => 'Ngày kết thúc thử việc',
			'official_start'   => 'Ngày làm việc chính thức',
			'trainer_id'       => 'Người hướng dẫn',
			'trainer_name'     => 'Người hướng dẫn',
			'city'             => 'Thành phố',
			'hometown'         => 'Quê quán',
			'id_number'        => 'Số CMT',
			'user_id'          => 'Người tạo',
			'password'         => 'Password',
			'note'             => 'Ghi chú',
			'picture'          => 'Ảnh chân dung',
			'apply_position'   => 'Vị trí ứng tuyển',
			'current_position' => 'Vị trí hiện tại',
		];
	}
}
