<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shipping_city".
 *
 * @property int    $id
 * @property string $name
 * @property string $code
 * @property string $short_code
 * @property int    $status
 * @property int    $type
 */
class ShippingCity extends \app\components\Model {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'shipping_city';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'code',
					'short_code',
				],
				'required',
			],
			[
				[
					'status',
					'type',
				],
				'integer',
			],
			[
				[
					'name',
					'code',
					'short_code',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'name'       => 'Tên thành phố',
			'code'       => 'Mã',
			'short_code' => 'Mã ngắn',
			'status'     => 'Status',
			'type'       => 'Type',
		];
	}
}
