<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shipping_district".
 *
 * @property int    $id
 * @property string $name
 * @property string $code
 * @property string $short_code
 * @property int    $status
 * @property int    $city_shipping
 * @property int    $type
 */
class ShippingDistrict extends \app\components\Model {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'shipping_district';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'code',
					'short_code',
					'city_shipping',
				],
				'required',
			],
			[
				[
					'status',
					'city_shipping',
					'type',
				],
				'integer',
			],
			[
				[
					'name',
					'code',
					'short_code',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'            => 'ID',
			'name'          => 'Tên quận/huyện',
			'code'          => 'Mã',
			'short_code'    => 'Mã ngắn',
			'status'        => 'Status',
			'city_shipping' => 'Thành phố',
			'type'          => 'Type',
		];
	}
}
