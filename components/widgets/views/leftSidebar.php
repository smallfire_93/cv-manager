<?php
use yii\helpers\Url;
use yii\widgets\Menu;

?>
<!-- END HEADER SEARCH BOX -->
<!-- BEGIN MEGA MENU -->
<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
<div class="page-header-menu">
	<div class="container">
		<!-- BEGIN HEADER SEARCH BOX -->
<!--		<form class="search-form" action="extra_search.html" method="GET">-->
<!--			<div class="input-group">-->
<!--				<input type="text" class="form-control" placeholder="Search" name="query">-->
<!--				<span class="input-group-btn">-->
<!--					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>-->
<!--					</span>-->
<!--			</div>-->
<!--		</form>-->
		<div class="hor-menu ">
			<?php
			echo Menu::widget([
				'items'           => [
//					[
//						'options'  => ['class' => 'sidebar-toggler-wrapper'],
//						'template' => '<div class="sidebar-toggler">
//				</div>',
//					],
//					[
//						'options'  => ['class' => 'sidebar-toggler-wrapper'],
//						'template' => '<form class="sidebar-search " action="extra_search.html" method="POST">
//					<a href="javascript:;" class="remove">
//						<i class="icon-close"></i>
//					</a>
//					<div class="input-group">
//						<input type="text" class="form-control" placeholder="Search...">
//						<span class="input-group-btn">
//							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
//							</span>
//					</div>
//				</form>',
//					],
					[
						'options'  => ['class' => 'menu-dropdown mega-menu-dropdown'],
						'url'      => ['/site/index'],
						'label'    => 'Dashboard',
						'template' => '<a class="dropdown-toggle" data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="{url}">{label}</a>',
					],
					[
						'options'  => ['class' => 'menu-dropdown mega-menu-dropdown'],
						'url'      => 'javascript:;',
						'label'    => 'Quản lý CV  <i class="fa fa-angle-down"></i>',
						'items'    => [
							[
								'label'   => 'Thêm mới CV',
								'url'     => ['/cv-info/create'],
//								'visible' => true,
								'options'  => ['class' => 'dropdown-submenu'],

							],
							[
								'label'   => 'Danh sách CV',
								'url'     => ['/cv-info/index'],
//								'visible' => true,
								'options'  => ['class' => 'dropdown-submenu'],
							],
						],
						'template' => '<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" class="dropdown-toggle" href="{url}">{label}</a>',
					],
					[
						'options'  => ['class' => 'menu-dropdown mega-menu-dropdown'],
						'url'      => 'javascript:;',
						'label'    => '
					Quản lý Sản phẩm
					<i class="fa fa-angle-down"></i>',
						'items'    => [
							[
								'label'   => 'Thêm mới sản phẩm',
								'url'     => ['/product/create'],
								'visible' => true,
							],
							[
								'label'   => 'Danh sách sản phẩm',
								'url'     => ['/product/index'],
								'visible' => true,
							],
							[
								'label'   => 'Lịch sử kho',
								'url'     => ['/stock-log'],
								'visible' => true,
							],
						],
						'template' => '<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" class="dropdown-toggle" href="{url}">{label}</a>',
					],
					[
						'options'  => ['class' => 'menu-dropdown mega-menu-dropdown'],
						'url'      => 'javascript:;',
						'label'    => '
					Quản lý khách hàng
					<i class="fa fa-angle-down"></i>',
						'items'    => [
							[
								'label'   => 'Thêm mới khách hàng',
								'url'     => ['/customer/create'],
								'visible' => true,
							],
							[
								'label'   => 'Danh sách khách hàng',
								'url'     => ['/customer/index'],
								'visible' => true,
							],
						],
						'template' => '<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" class="dropdown-toggle" href="{url}">{label}</a>',
					],
					[
						'options'  => ['class' => 'menu-dropdown mega-menu-dropdown'],
						'url'      => 'javascript:;',
						'label'    => '
					Quản lý đơn hàng
					<i class="fa fa-angle-down"></i>',
						'items'    => [
							[
								'label'   => 'Lên đơn',
								'url'     => ['/order/create'],
								'visible' => true,
							],
							[
								'label'   => 'Danh sách đơn hàng',
								'url'     => ['/order/index'],
								'visible' => true,
							],
						],
						'template' => '<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" class="dropdown-toggle" href="{url}">{label}</a>',
					],
					[
						'options'  => ['class' => 'menu-dropdown mega-menu-dropdown'],
						'url'      => 'javascript:;',
						'label'    => '
					Quản lý Cài đặt
					<i class="fa fa-angle-down"></i>',
						'items'    => [
							[
								'label'   => 'Cài đặt chung',
								'url'     => Url::to(['/setting']),
								'visible' => true,
							],
							[
								'label'   => 'Quản lý danh mục',
								'url'     => Url::to(['/category/index']),
								'visible' => true,
							],
							[
								'label'   => 'Quản lý thành phố',
								'url'     => Url::to(['/shipping-city/index']),
								'visible' => true,
							],
							[
								'label'   => 'Quản lý quận huyện',
								'url'     => Url::to(['/shipping-district/index']),
								'visible' => true,
							],
							[
								'label'   => 'Quản lý tin nhắn',
								'url'     => Url::to(['/sms-mt']),
								'visible' => true,
							],
							[
								'label'   => 'Quản lý quận huyện',
								'url'     => Url::to(['/setting']),
								'visible' => true,
							],
						],
						'template' => '<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" class="dropdown-toggle" href="{url}">{label}</a>',
					],
				],
				'encodeLabels'    => false,
				'submenuTemplate' => "\n<ul class='dropdown-menu pull-left'>\n{items}\n</ul>\n",
				'options'         => array(
					'class'              => 'nav navbar-nav',
					'data-keep-expanded' => "false",
					'data-auto-scroll'   => 'true',
					'data-slide-speed'   => "200",
				),
//				'linkTemplate'    => '<a href="{url}"><span class="title">{label}</span></a><span class="arrow "></span>',
				'activeCssClass'  => 'active',
				'activateParents' => true,
			])
			?>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
</div>
<!-- END SIDEBAR -->


