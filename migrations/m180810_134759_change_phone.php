<?php

use yii\db\Migration;

/**
 * Class m180810_134759_change_phone
 */
class m180810_134759_change_phone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('customer', 'phone', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180810_134759_change_phone cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180810_134759_change_phone cannot be reverted.\n";

        return false;
    }
    */
}
