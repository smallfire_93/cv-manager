<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m180801_131152_add_district
 */
class m180801_131152_add_district extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%shipping_district}}', [
			'id'            => Schema::TYPE_PK . '',
			'name'          => Schema::TYPE_STRING . '(255) NOT NULL',
			'code'          => Schema::TYPE_STRING . '(255) NOT NULL',
			'short_code'    => Schema::TYPE_STRING . '(255) NOT NULL',
			'status'        => Schema::TYPE_INTEGER . '(1) NULL',
			'city_shipping' => Schema::TYPE_INTEGER . '(255) NOT NULL',
			'type'          => Schema::TYPE_INTEGER . '(1) NULL',
		], $tableOptions);
		$this->insert('{{%shipping_district}}', [
			'id'            => '1',
			'name'          => 'THỊ XÃ CHÂU ĐỐC',
			'code'          => 'THỊ XÃ CHÂU ĐỐC-AGCD',
			'city_shipping' => '1',
			'short_code'    => 'AGCD',
			'status'        => '1',
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180801_131152_add_district cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180801_131152_add_district cannot be reverted.\n";

		return false;
	}
	*/
}
