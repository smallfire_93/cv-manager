<?php
use yii\db\Migration;

/**
 * Class m190928_080927_alter_note_order
 */
class m190928_080927_alter_note_order extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->alterColumn('order', 'note', $this->string(1000));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190928_080927_alter_note_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190928_080927_alter_note_order cannot be reverted.\n";

		return false;
	}
	*/
}
