<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m180712_095513_add_product
 */
class m180712_095513_add_product extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%category}}', [
			'id'         => Schema::TYPE_PK . '',
			'parent_id'  => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT "0"',
			'type'       => Schema::TYPE_INTEGER . '(11)',
			'sort_order' => Schema::TYPE_INTEGER . '(11)',
			'image'      => Schema::TYPE_STRING . '(255)',
			'name'      => Schema::TYPE_STRING . '(255)',
		], $tableOptions);
		$this->insert('{{%category}}', [
			'id'         => '1',
			'parent_id'  => '0',
			'type'       => '3',
			'sort_order' => '',
			'image'      => '',
			'name'      => 'Mỹ phẩm',
		]);
		$this->createTable('{{%product}}', [
			'id'           => Schema::TYPE_PK . '',
			'category_id'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'name'         => Schema::TYPE_STRING . '(255) NOT NULL',
			'code'         => Schema::TYPE_STRING . '(255)',
			'image'        => Schema::TYPE_STRING . '(255)',
			'in_stock'     => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT "1"',
			'base_price'   => Schema::TYPE_FLOAT . ' NOT NULL',
			'price'        => Schema::TYPE_FLOAT . ' NOT NULL',
			'description'  => Schema::TYPE_TEXT . ' NULL',
			'created_date' => Schema::TYPE_TIMESTAMP,
		], $tableOptions);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180712_095513_add_product cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180712_095513_add_product cannot be reverted.\n";

		return false;
	}
	*/
}
