<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m180725_031838_add_order
 */
class m180725_031838_add_order extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%order}}', [
			'id'                => Schema::TYPE_PK . '',
			'user_id'           => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'total_amount'      => Schema::TYPE_FLOAT . ' NOT NULL',
			'total_amount_real' => Schema::TYPE_FLOAT . ' NOT NULL',
			'discount'          => $this->float()->defaultValue(0),
			'note'              => Schema::TYPE_STRING . '(255) NULL',
			'created_date'      => Schema::TYPE_TIMESTAMP,
			'update_at'         => Schema::TYPE_DATETIME,
			'status'            => Schema::TYPE_INTEGER . ' NOT NULL',
			'update_by'         => Schema::TYPE_INTEGER . ' NULL',
			'customer_id'       => Schema::TYPE_INTEGER . ' NULL',
		], $tableOptions);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180725_031838_add_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180725_031838_add_order cannot be reverted.\n";

		return false;
	}
	*/
}
