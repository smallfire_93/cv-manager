<?php
use yii\db\Migration;

/**
 * Class m191111_161618_seed_attribute
 */
class m191111_161618_seed_attribute extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->insert('attribute', [
				'id'   => '1',
				'name' => 'Màu sắc',
			]);

		$this->insert('attribute', [
				'id'   => '2',
				'name' => 'Kích cỡ',
			]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m191111_161618_seed_attribute cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m191111_161618_seed_attribute cannot be reverted.\n";

		return false;
	}
	*/
}
