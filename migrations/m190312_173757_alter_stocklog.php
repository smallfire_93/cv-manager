<?php
use yii\db\Migration;

/**
 * Class m190312_173757_alter_stocklog
 */
class m190312_173757_alter_stocklog extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('stock_log', 'type', $this->integer());
		$this->addColumn('stock_log', 'order_id', $this->integer());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190312_173757_alter_stocklog cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190312_173757_alter_stocklog cannot be reverted.\n";

		return false;
	}
	*/
}
