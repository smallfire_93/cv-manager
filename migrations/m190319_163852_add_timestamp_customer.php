<?php
use yii\db\Migration;

/**
 * Class m190319_163852_add_timestamp_order
 */
class m190319_163852_add_timestamp_customer extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('customer', 'created_date', $this->timestamp());
		$this->addColumn('customer', 'update_date', $this->timestamp());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190319_163852_add_timestamp_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190319_163852_add_timestamp_order cannot be reverted.\n";

		return false;
	}
	*/
}
