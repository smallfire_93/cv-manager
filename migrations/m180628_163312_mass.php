<?php

use yii\db\Schema;
use yii\db\Migration;

class m180628_163312_mass extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%sms_mt}}', [
            'id' => Schema::TYPE_BIGPK . '',
            'phone' => Schema::TYPE_STRING . '(255) NOT NULL',
            'content' => Schema::TYPE_TEXT . '',
            'created_at' => Schema::TYPE_DATETIME . '',
            'cv_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'candidate_name' => Schema::TYPE_STRING . '(255)',
        ], $tableOptions);
    }

    public function safeDown()
    {

    }
}
