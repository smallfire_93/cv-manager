<?php
use yii\db\Migration;

/**
 * Class m180628_164522_update
 */
class m180628_164522_update extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('cv_info', 'is_sms', \yii\db\mysql\Schema::TYPE_INTEGER . '(1) DEFAULT 0');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180628_164522_update cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180628_164522_update cannot be reverted.\n";

		return false;
	}
	*/
}
