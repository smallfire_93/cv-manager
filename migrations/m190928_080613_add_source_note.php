<?php
use yii\db\Migration;

/**
 * Class m190928_080613_add_source_note
 */
class m190928_080613_add_source_note extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('order', 'source_note', $this->string(1000));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190928_080613_add_source_note cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190928_080613_add_source_note cannot be reverted.\n";

		return false;
	}
	*/
}
