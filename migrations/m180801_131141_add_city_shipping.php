<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m180801_131141_add_city_shipping
 */
class m180801_131141_add_city_shipping extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%shipping_city}}', [
			'id'         => Schema::TYPE_PK . '',
			'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'code'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'short_code' => Schema::TYPE_STRING . '(255) NOT NULL',
			'status'     => Schema::TYPE_INTEGER . '(1) NULL',
			'type'       => Schema::TYPE_INTEGER . '(1) NULL',
		], $tableOptions);
		$this->insert('{{%shipping_city}}', [
			'id'         => '1',
			'name'       => 'An giang',
			'code'       => 'AN_GIANG_AGG',
			'short_code' => 'AGG',
			'status'     => '1',
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180801_131141_add_city_shipping cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180801_131141_add_city_shipping cannot be reverted.\n";

		return false;
	}
	*/
}
