<?php
use yii\db\Migration;

/**
 * Class m180628_182958_update_cv
 */
class m180628_182958_update_cv extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->alterColumn('cv_info', 'interview_day', \yii\db\mssql\Schema::TYPE_DATETIME . ' NOT NULL');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180628_182958_update_cv cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180628_182958_update_cv cannot be reverted.\n";

		return false;
	}
	*/
}
