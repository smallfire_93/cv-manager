<?php

use yii\db\Migration;

/**
 * Class m180810_125308_add_shipping_fee
 */
class m180810_125308_add_shipping_fee extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'shipping_fee', $this->float()->null()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180810_125308_add_shipping_fee cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180810_125308_add_shipping_fee cannot be reverted.\n";

        return false;
    }
    */
}
