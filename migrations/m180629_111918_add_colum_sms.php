<?php
use yii\db\Migration;

/**
 * Class m180629_111918_add_colum_sms
 */
class m180629_111918_add_colum_sms extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('sms_mt', 'status', \yii\db\mysql\Schema::TYPE_INTEGER . ' NOT NULL');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180629_111918_add_colum_sms cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180629_111918_add_colum_sms cannot be reverted.\n";

		return false;
	}
	*/
}
