<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m180725_044236_add_order_item
 */
class m180725_044236_add_order_item extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%order_item}}', [
			'id'          => Schema::TYPE_PK . '',
			'order_id'    => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'product_id'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'quantity'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'total_price' => Schema::TYPE_FLOAT . ' NOT NULL',
		], $tableOptions);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180725_044236_add_order_item cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180725_044236_add_order_item cannot be reverted.\n";

		return false;
	}
	*/
}
