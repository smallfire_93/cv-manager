<?php
use yii\db\Migration;

/**
 * Class m191106_162422_add_level_2_product_manager_system
 */
class m191106_162422_add_level_2_product_manager_system extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('attribute', [
			'id'   => $this->primaryKey(),
			'name' => $this->string(255),
		], $tableOptions);
		$this->createTable('attribute_value', [
			'id'           => $this->primaryKey(),
			'attribute_id' => $this->integer(),
			'name'         => $this->string(),
		], $tableOptions);
		$this->createTable('product_attribute', [
			'id'                 => $this->primaryKey(),
			'attribute_value_id' => $this->integer(),
			'product_id'         => $this->integer(),
			'quantity'           => $this->integer(),
		], $tableOptions);
		$this->createTable('order_item_attribute', [
			'id'                 => $this->primaryKey(),
			'order_item_id'      => $this->integer(),
			'attribute_value_id' => $this->integer(),
			'product_id'         => $this->integer(),
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m191106_162422_add_level_2_product_manager_system cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m191106_162422_add_level_2_product_manager_system cannot be reverted.\n";

		return false;
	}
	*/
}
