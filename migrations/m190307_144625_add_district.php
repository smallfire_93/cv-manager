<?php
use yii\db\Migration;

/**
 * Class m190307_144625_add_district
 */
class m190307_144625_add_district extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('customer', 'shipping_city_id', $this->integer());
		$this->addColumn('customer', 'shipping_district_id', $this->integer());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190307_144625_add_district cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190307_144625_add_district cannot be reverted.\n";

		return false;
	}
	*/
}
