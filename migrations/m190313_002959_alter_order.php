<?php
use yii\db\Migration;

/**
 * Class m190313_002959_alter_order
 */
class m190313_002959_alter_order extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('order', 'code', $this->string());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190313_002959_alter_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190313_002959_alter_order cannot be reverted.\n";

		return false;
	}
	*/
}
