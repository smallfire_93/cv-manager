<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m180726_014722_add_stock_log
 */
class m180726_014722_add_stock_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	    $this->createTable('{{%stock_log}}', [
		    'id'          => Schema::TYPE_PK . '',
		    'product_id'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
		    'quantity'    => Schema::TYPE_INTEGER . ' NOT NULL',
		    'total_price' => Schema::TYPE_FLOAT . ' NULL',
		    'base_price' => Schema::TYPE_FLOAT . ' NULL',
	    ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180726_014722_add_stock_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180726_014722_add_stock_log cannot be reverted.\n";

        return false;
    }
    */
}
