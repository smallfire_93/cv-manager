<?php

use yii\db\Migration;

/**
 * Class m180812_102229_add_weight
 */
class m180812_102229_add_weight extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'weight', $this->float()->notNull()->defaultValue(0.5));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180812_102229_add_weight cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180812_102229_add_weight cannot be reverted.\n";

        return false;
    }
    */
}
