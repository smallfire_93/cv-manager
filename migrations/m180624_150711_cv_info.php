<?php

use yii\db\Schema;
use yii\db\Migration;

class m180624_150711_cv_info extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%cv_info}}',
            [
                'id'=> Schema::TYPE_PK.'',
                'name'=> Schema::TYPE_STRING.'(255)',
                'birthday'=> Schema::TYPE_DATE.'',
                'phone'=> Schema::TYPE_STRING.'(255) NOT NULL',
                'email'=> Schema::TYPE_STRING.'(255) NOT NULL',
                'cv'=> Schema::TYPE_STRING.'(255)',
                'address'=> Schema::TYPE_STRING.'(255)',
                'status'=> Schema::TYPE_TINYINT.'(4) NOT NULL',
                'gender'=> Schema::TYPE_INTEGER.'(11)',
                'interview_day'=> Schema::TYPE_DATE.'',
                'trial_start'=> Schema::TYPE_DATE.'',
                'trial_end'=> Schema::TYPE_DATE.'',
                'official_start'=> Schema::TYPE_DATE.'',
                'trainer_id'=> Schema::TYPE_INTEGER.'(11)',
                'trainer_name'=> Schema::TYPE_STRING.'(255)',
                'city'=> Schema::TYPE_INTEGER.'(11)',
                'hometown'=> Schema::TYPE_STRING.'(255)',
                'id_number'=> Schema::TYPE_INTEGER.'(11)',
                'user_id'=> Schema::TYPE_INTEGER.'(11)',
                'password'=> Schema::TYPE_STRING.'(1000)',
                'note'=> Schema::TYPE_TEXT.'',
                'picture'=> Schema::TYPE_STRING.'(255)',
                ],
            $tableOptions
        );
    
    }

    public function safeDown()
    {
        $this->dropTable('{{%cv_info}}');
    }
}
