<?php

use yii\db\Migration;

/**
 * Class m180806_113818_add_order_manager
 */
class m180806_113818_add_order_manager extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'created_by', $this->integer()->null());
        $this->addColumn('order', 'staff_name', $this->string()->null());
        $this->alterColumn('order', 'user_id', $this->integer()->null()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180806_113818_add_order_manager cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180806_113818_add_order_manager cannot be reverted.\n";

        return false;
    }
    */
}
