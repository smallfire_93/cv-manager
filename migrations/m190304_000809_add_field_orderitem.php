<?php
use yii\db\Migration;

/**
 * Class m190304_000809_add_field_orderitem
 */
class m190304_000809_add_field_orderitem extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('order_item', 'status', $this->integer());
		$this->addColumn('order_item', 'discount', $this->double());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190304_000809_add_field_orderitem cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190304_000809_add_field_orderitem cannot be reverted.\n";

		return false;
	}
	*/
}
