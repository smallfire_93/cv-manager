<?php
use yii\db\Migration;

/**
 * Class m191027_172728_add_create_date_stock_log
 */
class m191027_172728_add_create_date_stock_log extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('stock_log', 'created_at', $this->dateTime());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m191027_172728_add_create_date_stock_log cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m191027_172728_add_create_date_stock_log cannot be reverted.\n";

		return false;
	}
	*/
}
