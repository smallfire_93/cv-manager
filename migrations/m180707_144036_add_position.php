<?php
use yii\db\Migration;

/**
 * Class m180707_144036_add_position
 */
class m180707_144036_add_position extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('cv_info', 'apply_position', \yii\db\mysql\Schema::TYPE_STRING);
		$this->addColumn('cv_info', 'current_position', \yii\db\mysql\Schema::TYPE_STRING);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180707_144036_add_position cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180707_144036_add_position cannot be reverted.\n";

		return false;
	}
	*/
}
