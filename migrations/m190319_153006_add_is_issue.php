<?php
use yii\db\Migration;

/**
 * Class m190319_153006_add_is_issue
 */
class m190319_153006_add_is_issue extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('order', 'is_issue', $this->smallInteger(1));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190319_153006_add_is_issue cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190319_153006_add_is_issue cannot be reverted.\n";

		return false;
	}
	*/
}
