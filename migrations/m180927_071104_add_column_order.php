<?php
use yii\db\Migration;

/**
 * Class m180927_071104_add_column_order
 */
class m180927_071104_add_column_order extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('order', 'weight', $this->float());
		$this->addColumn('order', 'product_content', $this->string());
		$this->addColumn('order', 'is_customer_fee', $this->integer(1));
		$this->addColumn('order', 'collect_behalf', $this->float());
		$this->addColumn('order', 'service', $this->string());
		$this->addColumn('order', 'other_service', $this->string());
		$this->addColumn('order', 'is_customer_check', $this->integer(1));
		$this->addColumn('order', 'shipping_code', $this->string());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180927_071104_add_column_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180927_071104_add_column_order cannot be reverted.\n";

		return false;
	}
	*/
}
