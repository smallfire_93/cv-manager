<?php
use yii\db\Migration;

/**
 * Class m180709_145209_add_type_sms
 */
class m180709_145209_add_type_sms extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('sms_mt', 'type', $this->integer(1));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180709_145209_add_type_sms cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180709_145209_add_type_sms cannot be reverted.\n";

		return false;
	}
	*/
}
