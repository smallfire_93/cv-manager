<?php

use yii\db\Migration;

/**
 * Class m190911_143453_add_order_customer_status
 */
class m190911_143453_add_order_customer_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('customer', 'customer_type', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190911_143453_add_order_customer_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190911_143453_add_order_customer_status cannot be reverted.\n";

        return false;
    }
    */
}
