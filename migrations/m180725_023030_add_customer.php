<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m180725_023030_add_customer
 */
class m180725_023030_add_customer extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%customer}}', [
			'id'               => Schema::TYPE_PK . '',
			'name'             => Schema::TYPE_STRING . '(255) NOT NULL',
			'address'          => Schema::TYPE_STRING . '(255) NULL',
			'shipping_address' => Schema::TYPE_STRING . '(255) NULL',
			'note'             => Schema::TYPE_TEXT . '(1000) NULL',
			'email'            => Schema::TYPE_STRING . '(255) NULL',
			'phone'            => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'city_id'          => Schema::TYPE_INTEGER . '(11)',
			'user_id'          => Schema::TYPE_INTEGER . '(11)',
			'status'           => $this->integer(1)->defaultValue(0),
		], $tableOptions);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m180725_023030_add_customer cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180725_023030_add_customer cannot be reverted.\n";

		return false;
	}
	*/
}
