<?php
use yii\db\Migration;

/**
 * Class m190311_153946_add_shipping_village
 */
class m190311_153946_add_shipping_village extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('customer', 'shipping_village_id', $this->integer());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190311_153946_add_shipping_village cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m190311_153946_add_shipping_village cannot be reverted.\n";

		return false;
	}
	*/
}
