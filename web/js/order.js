$(document).on('change', '#orderform-phone', function() {
	$.ajax({
		url     : getInfoUrl,
		type    : "post",
		data    : {
			phone: $(this).val()
		},
		dataType: "json",
		success : function(data) {
			$("#district-id").append('');
			$("#village-id").append('');
			var customer = JSON.parse(data);
			$('#orderform-customer_name').val(customer.name);
			var district = new Option(customer.district, customer.shipping_district_id);
			var village  = new Option(customer.village, customer.shipping_village_id);
			/// jquerify the DOM object 'o' so we can use the html method
			$(district).html(customer.district);
			$(village).html(customer.village);
			$("#district-id").append(district);
			$("#village-id").append(village);

			$('#city-id').val(customer.shipping_city_id);
			$('#orderform-shipping_address').val(customer.shipping_address);
			$('#orderform-customer_note').val(customer.note);
			if(customer) {
				$('#orderform-customer_type').val(customer.customer_type);
			} else {
				$("#district-id").val('');
				$("#village-id").val('');
			}
		},
	});
});

//		Xử lí sự kiện click vào thêm form
$(".add-form").click(function() {
	var number = $(".items").find(".item-detail").length;
	console.log(number);
	var context = $("<div class='item-detail' >" + $(".item-detail").html() + "</div>");
	context.find("input,select").val("");
	context.find(".discount input").val(0);
	context.appendTo(".items").find('input:first').val(parseInt(number) + 1);
	$('.items .item-detail:last div:nth-child(3) select:first').attr('name', 'OrderItem[' + parseInt(number + 1) + '][product_id]');
	$('.items .item-detail:last div:nth-child(4) input:first').attr('name', 'OrderItem[' + parseInt(number + 1) + '][quantity]');
	$('.items .item-detail:last div:nth-child(5) input:first').attr('name', 'OrderItem[' + parseInt(number + 1) + '][discount]');
	return false;
});
$(document).on("change", ".category-select .form-control", function() {
	var context           = $(this).closest(".item-detail");
	var dependentDropdown = context.find("select[id='orderitem-product_id']");
	dependentDropdown.hide();
	dependentDropdown.parent().append('<i class="fa fa-spin fa-spinner"></i>');
	$.ajax({
		url    : orderItemUrl,
		type   : "post",
		data   : {
			category: $(this).val()
		},
		success: function(data) {
			setTimeout(function() {
				dependentDropdown.parent().find('.fa').remove();
				dependentDropdown.show();
				dependentDropdown.html(data);
			}, 500);
		}
	});
});
function numberFormat(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function originFormat(x) {
	return x.toString().replace(/,/g, "");
}
var sum = 0;
$(document).on("change", ".product-select .form-control", function() {
	var context  = $(this).closest(".item-detail");
	var codeGen  = context.find("input[name='code']");
	var quantity = context.find("input[id='orderitem-quantity']");
	var price    = context.find("input[name='OrderItem[total_price]']");
	$.ajax({
		url     : orderItemUrl,
		type    : "post",
		data    : {
			product: $(this).val()
		},
		dataType: "json",
		success : function(data) {
			console.log(data);
			context.find(".product-select .form-control option:selected").attr("data-price", data.product_price);
			codeGen.val(data.product_code);
			quantity.val(1);
			sub_total_price_event(context.find('.quantity input'));
			grand_total_quantity_event();
		}
	})
});
$(document).on("change", ".quantity .form-control", function() {
	sub_total_price_event($(this));
	grand_total_quantity_event();
});
$(document).on("keyup", ".quantity .form-control", function() {
	sub_total_price_event($(this));
	grand_total_quantity_event();
});
$(document).on("change", ".discount .form-control", function() {
	var quantity = $(this).closest(".item-detail").find(".quantity .form-control");
	sub_total_price_event($(quantity));
});
$(document).on("keyup", ".discount .form-control", function() {
	var quantity = $(this).closest(".item-detail").find(".quantity .form-control");
	sub_total_price_event($(quantity));
});
function grand_total_price_event() {
	var sub_total = 0;
	$("input[name='OrderItem[total_price]']").each(function() {
		sub_total += Number(originFormat($(this).val()));
	});
	//		var discount_value = parseFloat($(".value-item p:nth-child(2)").text());
	//		var grand_total    = sub_total - (sub_total * discount_value / 100);
	$(".value-item p:nth-child(1)").html(numberFormat(sub_total) + " vnđ");
	//		$(".value-item p:nth-child(3)").html(numberFormat(Math.round(grand_total) + " vnđ"));
}
function sub_total_price_event(selector) {
	var context            = selector.closest(".item-detail");
	var origin_price_value = context.find(".product-select select option:selected").data("price");

	if(origin_price_value != '' && origin_price_value != undefined) {
		var quantity        = selector.val();
		var discount        = context.find(".discount .form-control").val();
		var sub_total       = context.find("input[name='OrderItem[total_price]']");
		var sub_total_value = (origin_price_value * quantity) - discount;
		sub_total.val(numberFormat(sub_total_value));
		grand_total_price_event();
	}
}
function grand_total_quantity_event() {
	var sub_total = 0;
	$(".quantity input").each(function() {
		sub_total += Number($(this).val());
	});
	$(".value-quantity p:nth-child(1)").html(sub_total + " sp");
}