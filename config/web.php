<?php
$params = require __DIR__ . '/params.php';
$db     = require __DIR__ . '/db.php';
$config = [
	'id'         => 'basic',
	'basePath'   => dirname(__DIR__),
	'bootstrap'  => ['log'],
	'timeZone' => 'Asia/Ho_Chi_Minh',
	'aliases'    => [
		'@bower' => '@vendor/bower-asset',
		'@npm'   => '@vendor/npm-asset',
	],
	'components' => [
		'request'      => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'URcjh4sVJXeRqFD3bD8KTRl81-f3x5M8',
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
//		'user'         => [
//			'identityClass'   => 'app\models\User',
//			'enableAutoLogin' => true,
//			'enableSession' => true,
//		],
		'view' => [
			'theme' => [
				'pathMap' => [
					'@dektrium/user/views/security' => '@app/views/user'
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'setting'      => [
			'class' => 'navatech\setting\Setting',
		],
		'mailer'       => [
			'class'     => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'transport' => [
				'class'      => 'Swift_SmtpTransport',
				'host'       => 'smtp.gmail.com',
				'username'   => 'tuyendung@minhphonggroup.vn',
				'password'   => '09660982918801GGGH',
				'port'       => '587',
				'encryption' => 'tls',
			],
			//			'useFileTransport' => true,
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => [
						'error',
						'warning',
					],
				],
			],
		],
		'db'           => $db,
		/*
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
			],
		],
		*/
	],
	'modules'    => [
		'redactor' => [
			'class' => 'yii\redactor\RedactorModule',
			'uploadDir' => '@webroot/uploads',
			'imageAllowExtensions'=>['jpg','png','gif']
		],
		'setting'  => [
			'class'               => 'navatech\setting\Module',
			'controllerNamespace' => 'navatech\setting\controllers',
			'enableMultiLanguage' => false,
			//set true if navatech/yii2-multi-language installed and want to translate setting
		],
		'gridview' => [
			'class' => '\kartik\grid\Module',
		],
		'roxymce'  => [
			'class' => '\navatech\roxymce\Module',
		],
		'user'     => [
			'class' => 'dektrium\user\Module',
		],
	],
	'params'     => $params,
];
if(YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]      = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
	$config['bootstrap'][]    = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
}
return $config;
