<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 6/28/2018
 * Time: 11:34 PM
 */

namespace app\commands;

use app\models\CvInfo;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class SmsController extends Controller {

	public function actionIndex() {
		$models = CvInfo::find()->where(['is_sms' => CvInfo::NO_SMS])->all();
		foreach($models as $model) {
			$raw_message = Yii::$app->setting->first_message;
			$model->sendMessage($model, $raw_message);
			CvInfo::sendEmail($model);
		}
		return ExitCode::OK;
	}

	public function actionSecondSms() {
		$models = CvInfo::find()->where(['is_sms' => 1])->andWhere([
			'<=',
			'interview_day',
			date('Y-m-d 23:59:59'),
		])->andWhere([
			'>=',
			'interview_day',
			date('Y-m-d 00:00:00'),
		])->all();
		foreach($models as $model) {
			$raw_message = Yii::$app->setting->second_message;
			$model->sendMessage($model, $raw_message);
		}
	}
}