<?php

namespace app\controllers;

use app\models\Order;
use Yii;
use app\models\Customer;
use app\models\search\CustomerSearch;
use app\components\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller {

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Customer models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new CustomerSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionGetInfo() {
		if(isset($_POST['phone'])) {
			$phone_number = trim($_POST['phone'], ' ');
			$phone        = Customer::findOne(['phone' => $phone_number]);
			if($phone) {
				$attributes                         = $phone->attributes;
				$attributes['district']             = $phone->district->name??'';
				$attributes['village']              = $phone->village->name??'';
				$attributes['shipping_district_id'] = $phone->district->id??'';
				$attributes['shipping_village_id']  = $phone->village->id??'';
				$result                             = json_encode($attributes);
			} else {
				$result = false;
			}
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			echo json_encode($result);
		}
	}

	/**
	 * Displays a single Customer model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Customer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model               = new Customer();
		$model->created_date = date('Y-m-d H:i:s');
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		}
		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Customer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model        = $this->findModel($id);
		$dataProvider = new ActiveDataProvider([
			'query'      => Order::find()->where(['customer_id' => $id])->orderBy('id DESC'),
			'pagination' => [
				'pageSize' => 10,
			],
		]);
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		}
		return $this->render('update', [
			'model'        => $model,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Deletes an existing Customer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Customer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Customer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Customer::findOne($id)) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
