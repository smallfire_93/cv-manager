<?php

namespace app\controllers;

use app\components\Controller;
use SimpleXMLElement;
use SoapClient;
use stdClass;
use Yii;
use app\models\CvInfo;
use app\models\search\CvInfoSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CvInfoController implements the CRUD actions for CvInfo model.
 */
class CvInfoController extends Controller {

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all CvInfo models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new CvInfoSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single CvInfo model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	public function actionDownload($name) {
		$path = Yii::getAlias('@webroot') . '/uploads/cv_info/';
		$file = $path . $name;
		if(file_exists($file)) {
			Yii::$app->response->sendFile($file);
		}
	}

	/**
	 * Creates a new CvInfo model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new CvInfo();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			$img            = $model->uploadPicture('cv', 'cv_file');
			$model->user_id = 1;
			if($model->save()) {
				if($img !== false) {
					$path = $model->getPictureFile('cv');
					$img->saveAs($path);
				}
			}
			return $this->redirect([
				'index',
			]);
		}
		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing CvInfo model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		$oldCv = $model->cv;
		if($model->load(Yii::$app->request->post())) {
			$img = $model->uploadPicture('cv', 'cv_file');
			if($img == false) {
				$model->cv = $oldCv;
			}
			if($model->save()) {
				if($img !== false) {
					$path = $model->getPictureFile('image');
					$img->saveAs($path);
				}
			}
			return $this->redirect([
				'index',
			]);
		}
		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing CvInfo model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the CvInfo model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return CvInfo the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = CvInfo::findOne($id)) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
