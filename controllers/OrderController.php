<?php

namespace app\controllers;

use app\models\Customer;
use app\models\OrderForm;
use app\models\OrderItem;
use app\models\OrderItemSearch;
use app\models\Product;
use app\models\Province;
use app\models\ShippingCity;
use app\models\ShippingDistrict;
use app\models\StockLog;
use Yii;
use app\models\Order;
use app\models\search\OrderSearch;
use app\components\Controller;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller {

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Order models.
	 *
	 * @param null $order_type
	 *
	 * @return mixed
	 */
	public function actionIndex($orderType = null) {
		$searchModel  = new OrderSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $orderType);
		$numberOrder  = [];
		foreach(\yii\helpers\ArrayHelper::merge([0 => 'Tất cả'], Order::STATUS) as $key => $status) {
			$numberOrder[$key] = $searchModel->search(Yii::$app->request->queryParams, $key, true)->count;
		}
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'orderType'    => $orderType,
			'numberOrder'  => $numberOrder,
		]);
	}

	/**
	 * Displays a single Order model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Order model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model     = new OrderForm();
		$orderItem = new OrderItem();
		if(isset($_POST['OrderItem'])) {
			$model->load(Yii::$app->request->post());
			$order    = new Order();
			$customer = Customer::findOne(['phone' => $model->phone]);
			if($customer) {
				$order->customer_id             = $customer->id;
				$customer->shipping_city_id     = $model->city;
				$customer->shipping_district_id = $model->district;
				$customer->shipping_village_id  = $model->village;
				$customer->shipping_address     = $model->shipping_address;
				$customer->note                 = $model->customer_note;
				$customer->name                 = $model->customer_name;
			} else {
				$customer                       = new Customer();
				$customer->phone                = $model->phone;
				$customer->name                 = $model->customer_name;
				$customer->shipping_city_id     = $model->city;
				$customer->shipping_district_id = $model->district;
				$customer->shipping_village_id  = $model->village;
				$customer->shipping_address     = $model->shipping_address;
				$customer->note                 = $model->customer_note;
			}
			$customer->save();
			$order->status            = Order::STATUS_PENDING;
			$order->total_amount      = $model->total_amount ? $model->total_amount : 0;
			$order->total_amount_real = $model->total_amount_real ? $model->total_amount_real : 0;
			$order->weight            = $model->weight ? $model->weight : 0;
			$order->created_by        = \Yii::$app->user->id;
			$order->staff_name        = \Yii::$app->user->identity->username;
			$order->customer_id       = $customer->id;
			$order->note              = $model->note;
			$order->source_note       = $model->source_note;
			if($order->save()) {
				foreach($_POST['OrderItem'] as $item) {
					if($item['product_id']) {
						$orderItem = new OrderItem();
						$orderItem->setAttributes($item);
						$product = Product::findOne($item['product_id']);
						if($product) {
							if(isset($item['discount'])) {
								$discount        = $item['discount'];
								$model->discount += $item['discount'];
							} else {
								$discount = 0;
							}
							$orderItem->total_price   = $product->price * ($orderItem->quantity);
							$order->total_amount      += $product->price * ($orderItem->quantity);
							$order->total_amount_real += ($product->price * ($orderItem->quantity)) - $discount;
							$order->weight            += $product->weight;
						}
						if(($orderItem->quantity) <= 0) {
							$orderItem->quantity    = 0;
							$orderItem->total_price = 0;
						}
						$orderItem->order_id = $order->id;
						if($orderItem->save()) {
						}
					}
				}
				if($order->total_amount_real <= 0) {
					$order->delete();
					return $this->redirect(['index']);
				}
				$order->discount = $order->total_amount - $order->total_amount_real;
				$order->code     = date('d') . date('m') . date('Y') . 'MP' . $order->id;
				$order->save();
				Yii::$app->session->setFlash('success', 'Thành công');
				return $this->redirect([
					'index',
				]);
			} else {
				echo '<pre>';
				print_r($order->errors);
				die;
			}
		}
		return $this->render('create', [
			'model'     => $model,
			'orderItem' => $orderItem,
		]);
	}

	public function actionOrderItem() {
		if(isset($_POST['category'])) {
			$products = Product::findAll([
				'category_id' => $_POST['category'],
			]);
			$html     = '<option value>Chọn sản phẩm</option>';
			foreach($products as $product) {
				$html .= '<option value="' . $product->id . '">' . $product->name . '</option>';
			}
			return $html;
		} elseif(isset($_POST['product'])) {
			/**@var Product $product */
			$product = Product::find()->where(['id' => $_POST['product']])->one();
			$value   = [
				'product_code'  => $product->code,
				'product_price' => $product->price,
			];
			return json_encode($value);
		}
	}

	/**
	 * Updates an existing Order model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$order                    = $this->findModel($id);
		$model                    = new OrderForm();
		$model->phone             = $order->customer->phone;
		$model->customer_name     = $order->customer->name;
		$model->district          = $order->customer->shipping_district_id;
		$model->village           = $order->customer->shipping_village_id;
		$model->note              = $order->note;
		$model->customer_note     = $order->customer->note;
		$model->shipping_address  = $order->customer->shipping_address;
		$searchModel              = new OrderItemSearch();
		$dataProvider             = $searchModel->search(Yii::$app->request->queryParams, $id);
		$dataDistrict             = [];
		$dataVillage              = [];
		$model->status            = $order->status;
		$model->is_customer_check = $order->is_customer_check;
		$model->is_customer_fee   = $order->is_customer_fee;
		$model->service           = $order->service;
		$model->other_service     = $order->other_service;
		$model->collect_behalf    = $order->collect_behalf;
		$model->code              = $order->code;
		$model->shipping_code     = $order->shipping_code;
		$model->shipping_fee      = $order->shipping_fee;
		$model->staff_name        = $order->staff_name;
		$model->source_note       = $order->source_note;
		if($order->customer->district) {
			$dataDistrict = [$model->district => $order->customer->district->name];
		}
		if($order->customer->village) {
			$dataVillage = [$model->village => $order->customer->village->name];
		}
		if($order->customer->city) {
			$model->city = $order->customer->shipping_city_id;
		}
		if($model->load(Yii::$app->request->post())) {
			$transaction = Yii::$app->db->beginTransaction();
			$customer    = Customer::findOne(['phone' => $model->phone]);
			if($customer) {
				$order->customer_id             = $customer->id;
				$customer->shipping_city_id     = $model->city;
				$customer->shipping_district_id = $model->district;
				$customer->shipping_address     = $model->shipping_address;
				$customer->shipping_village_id  = $model->village;
				$customer->note                 = $model->customer_note;
				$customer->name                 = $model->customer_name;
			} else {
				$customer                       = new Customer();
				$customer->phone                = $model->phone;
				$customer->name                 = $model->customer_name;
				$customer->shipping_city_id     = $model->city;
				$customer->shipping_district_id = $model->district;
				$customer->shipping_village_id  = $model->village;
				$customer->shipping_address     = $model->shipping_address;
				$customer->note                 = $model->customer_note;
			}
			$customer->save();
			$order->customer_id       = $customer->id;
			$order->is_customer_check = $model->is_customer_check;
			$order->is_customer_fee   = $model->is_customer_fee;
			$order->service           = $model->service;
			$order->status            = $model->staff_name;
			$order->note              = $model->note;
			$order->other_service     = $model->other_service;
			$order->collect_behalf    = $model->collect_behalf;
			$order->code              = $model->code;
			$order->shipping_code     = $model->shipping_code;
			$order->shipping_fee      = $model->shipping_fee;
			$order->update_by         = Yii::$app->user->id;
			$order->update_at         = date('Y-m-d H:i:s');
			try {
				if($model->status != $order->status && !$order->is_issue) {
					if($model->status !== Order::STATUS_RETURNED && $model->status !== Order::STATUS_CANCEL) {
						$products = OrderItem::find()->where(['order_id' => $order->id])->all();
						foreach($products as $product) {
							$stockLog = new StockLog();
							$product_stock = Product::findOne($product->product_id);
							if($product_stock) {
								$stockLog->type         = StockLog::TYPE_ISSUE;
								$stockLog->quantity     = $product->quantity;
								$stockLog->product_id   = $product_stock->id;
								$stockLog->total_price  = $product->total_price;
								$stockLog->base_price   = $product_stock->base_price * $product->quantity;
								$stockLog->order_id     = $order->id;
								$stockLog->created_at = date('Y-m-d H:i:s');
								if(!$stockLog->save()){
									echo '<pre>';
									print_r($stockLog->errors);
									die;
								};
								$product_stock->updateAttributes(['in_stock' => ($product_stock->in_stock - $product->quantity)]);
							}
						}
						$order->is_issue = Order::ISSUED;
					}
				} elseif($model->status != $order->status && $order->is_issue) {
					if($model->status == Order::STATUS_RETURNED || $model->status == Order::STATUS_CANCEL) {
						$products = OrderItem::find()->where(['order_id' => $order->id])->all();
						foreach($products as $product) {
							$stockLog = new StockLog();
							$product_stock = Product::findOne($product->product_id);
							if($product_stock) {
								$stockLog->type         = StockLog::TYPE_RETURN;
								$stockLog->quantity     = $product->quantity;
								$stockLog->product_id   = $product_stock->id;
								$stockLog->total_price  = $product->total_price;
								$stockLog->base_price   = $product_stock->base_price * $product->quantity;
								$stockLog->order_id     = $order->id;
								$stockLog->created_at = date('Y-m-d H:i:s');
								$stockLog->save();
								$product_stock->updateAttributes(['in_stock' => $product_stock->in_stock + $product->quantity]);
							}
						}
						$order->is_issue = Order::NOT_ISSUE;
					}
				}
				$order->status = $model->status;
				if($order->save() && $order->status == Order::STATUS_SUCCESS) {
					$order->customer->updateAttributes(['status' => Customer::STATUS_BUY]);
				}
				$transaction->commit();
			} catch(Exception $e) {
				$transaction->rollBack();
			}
			return $this->redirect([
				'index',
			]);
		}
		return $this->render('update', [
			'order'        => $order,
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'model'        => $model,
			'dataDistrict' => $dataDistrict,
			'dataVillage'  => $dataVillage,
		]);
	}

	/**
	 * Deletes an existing Order model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Order model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Order the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Order::findOne($id)) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}

	public function actionGetDistrict() {
		$out = [];
		if(isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if($parents != null) {
				$city = $parents[0];
				$out  = self::getShippingDistrict($city);
				echo Json::encode([
					'output'   => $out,
					'selected' => '',
				]);
				return;
			}
		}
		echo Json::encode([
			'output'   => '',
			'selected' => '',
		]);
	}

	public function actionGetVillage() {
		$out = [];
		if(isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if($parents != null) {
				$district = $parents[0];
				$out      = self::getShippingVillage($district);
				echo Json::encode([
					'output'   => $out,
					'selected' => '',
				]);
				return;
			}
		}
		echo Json::encode([
			'output'   => '',
			'selected' => '',
		]);
	}

	public static function getShippingCity() {
		return ArrayHelper::map(Province::find()->where('type = 1')->all(), 'id', 'name');
	}

	public static function getShippingDistrict($city_id) {
		$districts    = Province::find()->where([
			'type'      => 2,
			'parent_id' => $city_id,
		])->all();
		$return_array = [];
		foreach($districts as $district) {
			$return_array[] = [
				'id'   => $district->id,
				'name' => $district->name,
			];
		}
		return $return_array;
	}

	public static function getShippingVillage($distric_id) {
		$villages     = Province::find()->where([
			'type'      => 3,
			'parent_id' => $distric_id,
		])->all();
		$return_array = [];
		foreach($villages as $village) {
			$return_array[] = [
				'id'   => $village->id,
				'name' => $village->name,
			];
		}
		return $return_array;
	}
}
