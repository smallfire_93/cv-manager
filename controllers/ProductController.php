<?php

namespace app\controllers;

use app\models\ProductAttribute;
use app\models\StockForm;
use app\models\StockLog;
use Yii;
use app\models\Product;
use app\models\search\ProductSearch;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Product models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new ProductSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Product model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Product model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model           = new Product();
		$modelAttributes = [new ProductAttribute()];
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			$img = $model->uploadPicture('image', 'product_img');
			if($model->save()) {
				if($img !== false) {
					$path = $model->getPictureFile('image');
					$img->saveAs($path);
				}
			}
			return $this->redirect(['index']);
		}
		return $this->render('create', [
			'model'             => $model,
			'modelAttributes' => $modelAttributes,
		]);
	}

	/**
	 * Updates an existing Product model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model    = $this->findModel($id);
		$oldImage = $model->image;
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			if($model->save()) {
				$img = $model->uploadPicture('image', 'product_img');
				//			$bill_img = $model->uploadPicture('bill_image', 'bill_img');
				if($img == false) {
					$model->image = $oldImage;
				} else {
					$path = $model->getPictureFile('image');
					$img->saveAs($path);
				}
				$stockLog              = new StockLog();
				$stockLog->product_id  = $model->id;
				$stockLog->base_price  = $model->base_price * $model->quantity;
				$stockLog->total_price = $model->base_price * $model->quantity;
				$stockLog->quantity    = $model->quantity;
				$stockLog->type        = StockLog::TYPE_RECEIPT;
				$stockLog->created_at  = date('Y-m-d H:i:s');
				$stockLog->save();
				return $this->redirect([
					'index',
				]);
			}
		}
		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionAdd($id) {
		$model   = new StockForm();
		$product = Product::findOne($id);
		if($model->load(Yii::$app->request->post())) {
			if($product) {
				$stockLog             = new StockLog();
				$stockLog->product_id = $product->id;
				// base price = total price in import product
				$stockLog->base_price  = $product->base_price * $model->quantity;
				$stockLog->total_price = $product->base_price * $model->quantity;
				$stockLog->quantity    = $model->quantity;
				$stockLog->type        = StockLog::TYPE_RECEIPT;
				$stockLog->created_at  = date('Y-m-d H:i:s');
				$stockLog->save();
				$product->updateAttributes(['in_stock' => $product->in_stock + $model->quantity]);
			}
			return $this->redirect([
				'index',
			]);
		}
		return $this->render('add', [
			'model'   => $model,
			'product' => $product,
		]);
	}

	/**
	 * Deletes an existing Product model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Product::findOne($id)) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
